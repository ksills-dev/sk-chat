#include <skchatapp.hpp>

#include <iostream>

#include <QApplication>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QMainWindow>
#include <QMenuBar>
#include <QObject>
#include <QPushButton>
#include <QTabBar>
#include <QTextEdit>
#include <QVBoxLayout>
#include <QWidget>

#include <verdigris/wobjectdefs.h>

#include <nlohmann/json.hpp>

#include <cryptopp/cryptlib.h>

int main(int argc, char **argv) {
  SkChatApp app{argc, argv};
  app.exec();
}
