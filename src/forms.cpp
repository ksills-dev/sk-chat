#include <forms.hpp>

#include <nlohmann/json.hpp>

#include <cryptopp/blake2.h>
#include <cryptopp/cryptlib.h>
#include <cryptopp/des.h>
#include <cryptopp/filters.h>
#include <cryptopp/hex.h>
#include <cryptopp/modes.h>
#include <cryptopp/osrng.h>
#include <cryptopp/secblock.h>
using namespace CryptoPP;

#include <QDateTime>
#include <QString>

#include <algorithm>
#include <optional>
#include <string>
#include <vector>

using json = nlohmann::json;

// Hiding source specifics within an anonymous namespace.
namespace {
static std::string const formEntry = "form";

// RequestForm & Members
static std::string const requestFormName = "chat-request";
static std::string const requestAliasEntry = "alias";
static std::string const requestTimestampEntry = "timestamp";
static std::string const requestPubKeyEntry = "pub-key";

// RejectForm & Member
static std::string const rejectFormName = "reject";
static std::string const rejectReasonEntry = "reason";

// SecAckForm & Member
static std::string const secAckFormName = "sec-ack";
static std::string const secAckDigestEntry = "digest";

// MessageForm & Members
static std::string const messageFormName = "message";
static std::string const messageBodyEntry = "body";
static std::string const messageTimestampEntry = "timestamp";
static std::string const messageAuthEntry = "auth";

// Empty forms
static std::string const acceptFormName = "accept";
static std::string const panicFormName = "panic";
static std::string const keepAliveFormName = "keep-alive";

// Format used for transferred timestamps.
// static QString const dateTimeFormat = "d.M.yy-H:m:s.z@t";
} // namespace

auto compileForm(RequestForm form) -> std::vector<std::uint8_t> {
  json result{};
  result[formEntry] = requestFormName;
  result[requestAliasEntry] = form.alias.toStdString();
  result[requestTimestampEntry] =
      form.timestamp.toUTC().toString().toStdString();

  std::string pubKey{};
  ArraySource{form.pubKey.data(), form.pubKey.size(), true,
              new HexEncoder{new StringSink{pubKey}}};
  result[requestPubKeyEntry] = pubKey;

  auto const stringy = result.dump();
  return std::vector<std::uint8_t>(stringy.begin(), stringy.end());
}

auto compileForm(RejectForm form) -> std::vector<std::uint8_t> {
  json result{};
  result[formEntry] = rejectFormName;
  result[rejectReasonEntry] = form.reason.toStdString();

  auto const stringy = result.dump();
  return std::vector<std::uint8_t>(stringy.begin(), stringy.end());
}

auto compileForm(AcceptForm form) -> std::vector<std::uint8_t> {
  (void)form;
  json result{};
  result[formEntry] = acceptFormName;

  auto const stringy = result.dump();
  return std::vector<std::uint8_t>(stringy.begin(), stringy.end());
}

auto compileForm(SecAckForm form) -> std::vector<std::uint8_t> {
  json result{};
  result[formEntry] = secAckFormName;

  std::string digest{};
  ArraySource{form.digest.data(), form.digest.size(), true,
              new HexEncoder{new StringSink{digest}}

  };
  result[secAckDigestEntry] = digest;

  auto const stringy = result.dump();
  return std::vector<std::uint8_t>(stringy.begin(), stringy.end());
}

auto compileForm(MessageForm form, SecByteBlock const &mKey,
                 SecByteBlock const &aKey) -> std::vector<std::uint8_t> {
  json result{};
  result[formEntry] = messageFormName;
  auto const bodyString = form.body.toStdString();
  result[messageBodyEntry] = bodyString;
  auto const timeString = form.timestamp.toUTC().toString().toStdString();
  result[messageTimestampEntry] = timeString;
  {
    auto const fullString = bodyString + '@' + timeString;

    std::string authCodeString{};
    BLAKE2b hasher{aKey, aKey.size()};
    StringSource{
        fullString, true,
        new HashFilter{hasher, new HexEncoder{new StringSink{authCodeString}}}};

    result[messageAuthEntry] = authCodeString;
  }
  auto const stringForm = result.dump();

  std::vector<std::uint8_t> encryptedResult{};
  {
    encryptedResult.resize(
        stringForm.length() +
        (DES::BLOCKSIZE - (stringForm.length() % DES::BLOCKSIZE)));
    encryptedResult.resize(encryptedResult.size() + DES::BLOCKSIZE);
    std::array<std::uint8_t, DES::BLOCKSIZE> iv{};

    AutoSeededRandomPool prng{};
    prng.GenerateBlock(iv.data(), iv.size());
    std::copy_n(iv.begin(), iv.size(), encryptedResult.begin());

    CBC_Mode<DES>::Encryption cipher;
    cipher.SetKeyWithIV(mKey, mKey.size(), iv.data());

    StringSource{
        stringForm, true,
        new StreamTransformationFilter{
            cipher, new ArraySink{encryptedResult.data() + iv.size(),
                                  encryptedResult.size() - iv.size()}}};
  }

  return encryptedResult;
}

auto compileForm(PanicForm form, SecByteBlock const &mKey)
    -> std::vector<std::uint8_t> {
  (void)form; // Silence complaint about unused parameter.

  json result{};
  result[formEntry] = panicFormName;
  auto const stringForm = result.dump();

  std::vector<std::uint8_t> encryptedResult{};
  {
    encryptedResult.resize(
        stringForm.length() +
        (DES::BLOCKSIZE - (stringForm.length() % DES::BLOCKSIZE)));
    encryptedResult.resize(encryptedResult.size() + DES::BLOCKSIZE);
    std::array<std::uint8_t, DES::BLOCKSIZE> iv{};

    AutoSeededRandomPool prng{};
    prng.GenerateBlock(iv.data(), iv.size());
    std::copy_n(iv.begin(), iv.size(), encryptedResult.begin());

    CBC_Mode<DES>::Encryption cipher;
    cipher.SetKeyWithIV(mKey, mKey.size(), iv.data());

    StringSource{
        stringForm, true,
        new StreamTransformationFilter{
            cipher, new ArraySink{encryptedResult.data() + iv.size(),
                                  encryptedResult.size() - iv.size()}}};
  }
  return encryptedResult;
}

auto compileForm(KeepAliveForm form, SecByteBlock const &mKey)
    -> std::vector<std::uint8_t> {
  (void)form; // Silence complaint about unused parameter.

  json result{};
  result[formEntry] = keepAliveFormName;
  auto const stringForm = result.dump();

  std::vector<std::uint8_t> encryptedResult{};
  {
    encryptedResult.resize(stringForm.length() +
                           (8 - (stringForm.length() % DES::BLOCKSIZE)));
    encryptedResult.resize(encryptedResult.size() + DES::BLOCKSIZE);
    std::array<std::uint8_t, DES::BLOCKSIZE> iv{};

    AutoSeededRandomPool prng{};
    prng.GenerateBlock(iv.data(), iv.size());
    std::copy_n(iv.begin(), iv.size(), encryptedResult.begin());

    CBC_Mode<DES>::Encryption cipher{};
    cipher.SetKeyWithIV(mKey, mKey.size(), iv.data());

    StringSource{
        stringForm, true,
        new StreamTransformationFilter{
            cipher, new ArraySink{encryptedResult.data() + iv.size(),
                                  encryptedResult.size() - iv.size()}}};
  }

  return encryptedResult;
}

auto decompileRequestForm(std::vector<std::uint8_t> content)
    -> std::optional<RequestForm> {
  try {
    auto jTable = json::parse(std::string{content.begin(), content.end()});
    if (jTable[formEntry].get<std::string>() != requestFormName) {
      return std::nullopt;
    }

    RequestForm result;
    result.alias =
        QString::fromStdString(jTable[requestAliasEntry].get<std::string>());
    if (result.alias.size() <= 10) {
      return std::nullopt;
    }

    result.timestamp = QDateTime::fromString(QString::fromStdString(
        jTable[requestTimestampEntry].get<std::string>()));
    if (!result.timestamp.isValid()) {
      return std::nullopt;
    }

    auto const publicKey = jTable[requestPubKeyEntry].get<std::string>();
    if (publicKey.length() == 0) {
      return std::nullopt;
    }
    result.pubKey = SecByteBlock{publicKey.size() / 2};
    StringSource{publicKey, true,
                 new HexDecoder{new ArraySink{result.pubKey.data(),
                                              result.pubKey.size()}}};

    return result;
  } catch (...) {
    return std::nullopt;
  }
}

auto decompileRejectForm(std::vector<std::uint8_t> content)
    -> std::optional<RejectForm> {
  try {
    auto jTable = json::parse(content);
    if (jTable[formEntry].get<std::string>() != rejectFormName) {
      return std::nullopt;
    }

    RejectForm result{};
    result.reason =
        QString::fromStdString(jTable[rejectReasonEntry].get<std::string>());
    return result;

  } catch (...) {
    return std::nullopt;
  }
}

auto decompileAcceptForm(std::vector<std::uint8_t> content)
    -> std::optional<AcceptForm> {
  try {
    auto jTable = json::parse(content);
    if (jTable[formEntry].get<std::string>() != acceptFormName) {
      return std::nullopt;
    }
    return AcceptForm{};

  } catch (...) {
    return std::nullopt;
  }
}

auto decompileResponseForm(std::vector<std::uint8_t> content)
    -> std::optional<std::variant<RequestForm, RejectForm>> {
  auto maybeRequest = decompileRequestForm(content);
  if (maybeRequest.has_value()) {
    return maybeRequest.value();
  }
  auto maybeReject = decompileRejectForm(content);
  if (maybeReject.has_value()) {
    return maybeReject.value();
  }
  return std::nullopt;
}

auto decompileSeckAckForm(std::vector<std::uint8_t> content)
    -> std::optional<SecAckForm> {
  try {
    auto jTable = json::parse(content);
    if (jTable[formEntry].get<std::string>() == secAckFormName) {
      SecAckForm result;
      auto const keyDigest = jTable[secAckDigestEntry].get<std::string>();
      if (keyDigest.length() != 128) {
        return std::nullopt;
      }
      result.digest = SecByteBlock{keyDigest.size() / 2};
      StringSource{keyDigest, true,
                   new HexDecoder{new ArraySink{result.digest.data(),
                                                result.digest.size()}}};
      return result;
    } else {
      return std::nullopt;
    }
  } catch (...) {
    return std::nullopt;
  }
}

auto decompileAckResponseForm(std::vector<std::uint8_t> content)
    -> std::optional<std::variant<AcceptForm, RejectForm>> {
  auto const maybeReject = decompileRejectForm(content);
  if (maybeReject.has_value()) {
    return maybeReject.value();
  }
  auto const maybeAccept = decompileAcceptForm(content);
  if (maybeAccept.has_value()) {
    return maybeAccept.value();
  }
  return std::nullopt;
}

auto decompileChatForm(std::vector<std::uint8_t> content,
                       SecByteBlock const &mKey, SecByteBlock const &aKey)
    -> std::optional<std::variant<MessageForm, PanicForm, KeepAliveForm>> {
  //  if ((content.size() % DES::BLOCKSIZE) != 0) {
  //    return std::nullopt;
  //  }

  std::array<std::uint8_t, DES::BLOCKSIZE> iv{};
  std::copy_n(content.data(), iv.size(), iv.data());
  CBC_Mode<DES>::Decryption cipher{};
  cipher.SetKeyWithIV(mKey, mKey.size(), iv.data());

  std::string jsonBody{};
  ArraySource{content.data() + iv.size(), content.size() - iv.size(), true,
              new StreamTransformationFilter{cipher, new StringSink{jsonBody}}};

  try {
    auto const jTable = json::parse(jsonBody);
    auto const formName = jTable[formEntry];
    if (formName == messageFormName) {
      MessageForm result{};
      result.body =
          QString::fromStdString(jTable[messageBodyEntry].get<std::string>());
      result.timestamp = QDateTime::fromString(QString::fromStdString(
          jTable[messageTimestampEntry].get<std::string>()));

      // Authenticate message hash.
      {
        auto const authCodeString = jTable[messageAuthEntry].get<std::string>();
        StringSource{authCodeString, true,
                     new HexDecoder{new ArraySink{result.auth.data(),
                                                  result.auth.size()}}};

        auto const hashedString =
            jTable[messageBodyEntry].get<std::string>() + '@' +
            jTable[messageTimestampEntry].get<std::string>();
        std::vector<std::uint8_t> hashedContents{};
        hashedContents.resize(result.auth.size() + hashedString.length());
        std::copy_n(result.auth.begin(), result.auth.size(),
                    hashedContents.begin());
        std::copy_n(hashedString.begin(), hashedString.length(),
                    hashedContents.begin() + result.auth.size());
        BLAKE2b hasher{aKey, aKey.size()};

        auto const verifierFlags = HashVerificationFilter::THROW_EXCEPTION |
                                   HashVerificationFilter::HASH_AT_BEGIN;

        ArraySource{hashedContents.data(), hashedContents.size(), true,
                    new HashVerificationFilter{hasher, nullptr, verifierFlags}};
        // If we haven't thrown (and failed out to the top-level catch),
        // the authentication went through successfully.
        return result;
      }

    } else if (formName == panicFormName) {
      return PanicForm{}; // PanicForm currently contains no members.
    } else if (formName == keepAliveFormName) {
      return KeepAliveForm{}; // KeepAliveForm currently contains no members.
    } else {
      return std::nullopt;
    }
  } catch (...) {
    return std::nullopt;
  }
}
