#include <handshaker.hpp>

#include <framestripper.hpp>
#include <skchatapp.hpp>

#include <QDateTime>
#include <QString>
#include <QTcpServer>
#include <QTimer>

#include <verdigris/wobjectimpl.h>

#include <cryptopp/blake2.h>
#include <cryptopp/des.h>
#include <cryptopp/dh.h>
#include <cryptopp/integer.h>
#include <cryptopp/osrng.h>
#include <cryptopp/scrypt.h>
using namespace CryptoPP;

#include <chrono>
#include <iostream>
#include <string>
#include <utility>

namespace {
static Integer const goodPrime{
    "0x"
    "FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD1"
    "29024E088A67CC74020BBEA63B139B22514A08798E3404DD"
    "EF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245"
    "E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7ED"
    "EE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3D"
    "C2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F"
    "83655D23DCA3AD961C62F356208552BB9ED529077096966D"
    "670C354E4ABC9804F1746C08CA18217C32905E462E36CE3B"
    "E39E772C180E86039B2783A2EC07A28FB5C55DF06F4C52C9"
    "DE2BCBF6955817183995497CEA956AE515D2261898FA0510"
    "15728E5A8AAAC42DAD33170D04507A33A85521ABDF1CBA64"
    "ECFB850458DBEF0A8AEA71575D060C7DB3970F85A6E1E4C7"
    "ABF5AE8CDB0933D71E8C94E04A25619DCEE3D2261AD2EE6B"
    "F12FFA06D98A0864D87602733EC86A64521F2B18177B200C"
    "BBE117577A615D6C770988C0BAD946E208E24FA074E5AB31"
    "43DB5BFCE0FD108E4B82D120A92108011A723C12A787E6D7"
    "88719A10BDBA5B2699C327186AF4E23C1A946834B6150BDA"
    "2583E9CA2AD44CE8DBBBC2DB04DE8EF92E8EFC141FBECAA6"
    "287C59474E6BC05D99B2964FA090C3A2233BA186515BE7ED"
    "1F612970CEE2D7AFB81BDD762170481CD0069127D5B05AA9"
    "93B4EA988D8FDDC186FFB7DC90A6C08F4DF435C934063199"
    "FFFFFFFFFFFFFFFF"};

static int const longTimeout = 20000;
static int const handshakeTimeouts = 2000;
static std::uint16_t const defaultDispatchPort = 1337;

auto generateSideKey(QString alias, SecByteBlock key, QDateTime timestamp)
    -> Integer {
  BLAKE2b hasher{};
  SecByteBlock result{BLAKE2b::DIGESTSIZE};

  auto fixedAlias = alias.toStdString();
  auto fixedTimestamp = timestamp.toUTC().toString().toStdString();

  hasher.Update(reinterpret_cast<std::uint8_t const *>(fixedAlias.data()),
                fixedAlias.size());
  hasher.Update(key.data(), key.size());
  hasher.Update(reinterpret_cast<std::uint8_t const *>(fixedTimestamp.data()),
                fixedTimestamp.size());
  hasher.Final(result.data());

  return Integer{result.data(), result.size()};
}

auto generateIdKey(Integer sideKey1, Integer sideKey2) -> SecByteBlock {
  SecByteBlock sideKey1Arr{BLAKE2b::DIGESTSIZE};
  sideKey1.Encode(sideKey1Arr.data(), sideKey1Arr.size());
  SecByteBlock sideKey2Arr{BLAKE2b::DIGESTSIZE};
  sideKey2.Encode(sideKey2Arr.data(), sideKey2Arr.size());

  SecByteBlock result{};
  auto const selector = (sideKey1.Compare(sideKey2) < 0);
  result += selector ? sideKey2Arr : sideKey1Arr;
  result += selector ? sideKey1Arr : sideKey2Arr;

  return result;
}

auto generateAuthDigests(SecByteBlock key)
    -> std::pair<SecByteBlock, SecByteBlock> {
  SecByteBlock once{BLAKE2b::DIGESTSIZE};
  SecByteBlock twice{BLAKE2b::DIGESTSIZE};

  BLAKE2b hasher{};
  ArraySource{key.data(), key.size(), true,
              new HashFilter{hasher, new ArraySink{once.data(), once.size()}}};
  ArraySource{
      once.data(), once.size(), true,
      new HashFilter{hasher, new ArraySink{twice.data(), twice.size()}}};

  return std::make_pair(once, twice);
}

auto assignSubKeys(SecByteBlock sKey, ChannelData &first, ChannelData &second)
    -> void {
  first.sKey = SecByteBlock{sKey.data(), 32};
  first.mKey = SecByteBlock{first.sKey.data(), 8};
  first.aKey = SecByteBlock{first.sKey.data() + 8, 8};
  DES::CorrectKeyParityBits(first.mKey.data());
  DES::CorrectKeyParityBits(first.aKey.data());

  second.sKey = SecByteBlock{sKey.data() + 32, 32};
  second.mKey = SecByteBlock{second.sKey.data(), 8};
  second.aKey = SecByteBlock{second.sKey.data() + 8, 8};
  DES::CorrectKeyParityBits(second.mKey.data());
  DES::CorrectKeyParityBits(second.aKey.data());
}

} // namespace

W_OBJECT_IMPL(Handshaker)

// Initiator
Handshaker::Handshaker(UserRequestInfo userInfo)
    : _myPubKey{}, _myPrivKey{}, _partnerPubKey{}, _state{State::InitRequest} {
  _partnerAddress = userInfo.target;
  _myAlias = userInfo.form.alias;
  _myTime = userInfo.form.timestamp;
  _passHash = userInfo.passHash;

  _socket = new QTcpSocket();
  _socket->bind();
  _socket->connectToHost(_partnerAddress, 1337);
  QObject::connect(_socket, &QTcpSocket::connected, this,
                   &Handshaker::sendInitRequest);

  QObject::connect(&_timeout, &QTimer::timeout, this,
                   &Handshaker::handleTimeout);
  _timeout.setInterval(longTimeout);
  _timeout.start();
}

// Receiver
Handshaker::Handshaker(QTcpSocket *socket)
    : _myPubKey{}, _myPrivKey{}, _partnerPubKey{}, _state{State::RecvRequest} {
  _partnerAddress = socket->peerAddress();

  _socket = socket;
  _socket->setParent(nullptr);
  QObject::connect(_socket, &QTcpSocket::readyRead, this,
                   &Handshaker::handlePacket);
  QObject::connect(_socket, &QTcpSocket::disconnected, this,
                   &Handshaker::close);

  QObject::connect(&_timeout, &QTimer::timeout, this,
                   &Handshaker::handleTimeout);
  _timeout.setInterval(handshakeTimeouts);
  _timeout.start();
}

auto Handshaker::close() -> void {
  _closed = true;

  _timeout.disconnect();
  if (_socket != nullptr) {
    _socket->disconnect();
    _socket->deleteLater();
  }

  disconnect();
  deleteLater();
}

auto Handshaker::handleDisconnect() -> void {
  if (!_closed)
    return emit onFailure(HandshakeFailCause::NetworkErr);
  close();
}

auto Handshaker::handleTimeout() -> void {
  if (!_closed)
    return emit onFailure(HandshakeFailCause::NetworkErr);
  close();
}

auto Handshaker::sendInitRequest() -> void {
  _timeout.stop();

  QObject::disconnect(_socket, &QTcpSocket::connected, this,
                      &Handshaker::sendInitRequest);
  QObject::connect(_socket, &QTcpSocket::readyRead, this,
                   &Handshaker::handlePacket);
  QObject::connect(_socket, &QTcpSocket::disconnected, this,
                   &Handshaker::handleDisconnect);

  prepareDhExchange();

  auto const content =
      generateFrame(compileForm(RequestForm{_myAlias, _myTime, _myPubKey}));
  if (_socket->write(reinterpret_cast<char const *>(content.data()),
                     static_cast<int>(content.size())) == -1) {
    emit onFailure(HandshakeFailCause::NetworkErr);
    close();
  }
  QObject::disconnect(_socket, &QTcpSocket::connected, this,
                      &Handshaker::sendInitRequest);
  _timeout.start();
}

auto Handshaker::handlePacket() -> void {
  _timeout.stop();
  do {
    auto const count = _socket->bytesAvailable();
    std::vector<std::uint8_t> content{};
    content.resize(static_cast<std::size_t>(count));
    _socket->read(reinterpret_cast<char *>(content.data()), count);
    _frameFilter.push(content);

    if (_frameFilter.isReady()) {
      switch (_state) {
      case State::InitRequest:
        handleInitRequest();
        break;
      case State::InitSecAck:
        handleInitSecAck();
        break;
      case State::InitAuth:
        handleInitAuth();
        break;
      case State::RecvRequest:
        handleRecvRequest();
        break;
      case State::RecvSecAck:
        handleRecvSecAck();
        break;
      }
    }
  } while (!_closed && (_socket->bytesAvailable() || _frameFilter.isReady()));
  if (!_closed)
    _timeout.start();
}

auto Handshaker::handleInitRequest() -> void {
  auto const maybeRequestForm =
      decompileResponseForm(_frameFilter.pop().value());
  if (!maybeRequestForm.has_value()) {
    emit onFailure(HandshakeFailCause::NetworkErr);
    close();
    return;
  } else if (std::holds_alternative<RejectForm>(maybeRequestForm.value())) {
    emit onFailure(HandshakeFailCause::Rejected);
    close();
    return;
  }

  auto const requestForm = std::get<RequestForm>(maybeRequestForm.value());
  auto const intPubKey =
      Integer{requestForm.pubKey.data(), requestForm.pubKey.size()};
  if (!(intPubKey.Compare(2) == 1) ||
      !(intPubKey.Compare(goodPrime - 2) == -1)) {
    auto const content =
        generateFrame(compileForm(RejectForm{"Bad Public Key."}));
    _socket->write(reinterpret_cast<char const *>(content.data()),
                   static_cast<int>(content.size()));
    _socket->flush();
    close();
    return;
  }
  auto const response = emit promptUserIncoming(_partnerAddress, requestForm);
  if (!response) {
    auto const content =
        generateFrame(compileForm(RejectForm{"User declined."}));
    _socket->write(reinterpret_cast<char const *>(content.data()),
                   static_cast<int>(content.size()));
    _socket->flush();
    close();
    return;
  }

  _partnerAlias = requestForm.alias;
  _partnerTime = requestForm.timestamp;
  _partnerPubKey = requestForm.pubKey;

  _state = State::InitSecAck;
  if (_frameFilter.isReady()) {
    handleInitSecAck();
  }
  _timeout.setInterval(handshakeTimeouts);
}

auto Handshaker::handleInitSecAck() -> void {
  // Check for Partner SecAck
  auto const maybeSecAck = decompileSeckAckForm(_frameFilter.pop().value());
  if (!maybeSecAck.has_value()) {
    emit onFailure(HandshakeFailCause::NetworkErr);
    close();
    return;
  }

  auto const partnerDigest = maybeSecAck.value().digest;

  // Generate shared secret
  SecByteBlock secret{_dhSolver.AgreedValueLength()};
  _dhSolver.Agree(secret.data(), _myPrivKey, _partnerPubKey, false);
  establishSKey(secret);

  // Auth partner's digest
  _authDigests = generateAuthDigests(_sKey);
  if (partnerDigest != _authDigests.second) {
    auto const rejectContent =
        generateFrame(compileForm(RejectForm{"Failed Authentication."}));
    _socket->write(reinterpret_cast<char const *>(rejectContent.data()),
                   static_cast<int>(rejectContent.size()));
    _socket->flush();
    emit onFailure(HandshakeFailCause::FailedAuth);
    close();
    return;
  }

  // Send SeckAck
  auto const authContent =
      generateFrame(compileForm(SecAckForm{_authDigests.first}));
  if (_socket->write(reinterpret_cast<char const *>(authContent.data()),
                     static_cast<int>(authContent.size())) == -1) {
    emit onFailure(HandshakeFailCause::NetworkErr);
    close();
    return;
  }
  _socket->flush();

  _state = State::InitAuth;
  if (_frameFilter.isReady()) {
    handleInitAuth();
  }
}

auto Handshaker::handleInitAuth() -> void {
  auto const maybeAckResponse =
      decompileAckResponseForm(_frameFilter.pop().value());
  if (!maybeAckResponse.has_value()) {
    emit onFailure(HandshakeFailCause::NetworkErr);
    close();
    return;
  }
  if (std::holds_alternative<RejectForm>(maybeAckResponse.value())) {
    emit onFailure(HandshakeFailCause::FailedAuth);
    close();
    return;
  }

  auto room = createRoom();
  assignSubKeys(_sKey, room->_outChannel, room->_inChannel);
  emit onSuccess(room);
  close();
}

auto Handshaker::handleRecvRequest() -> void {
  auto const maybeRequestForm =
      decompileRequestForm(_frameFilter.pop().value());
  if (!maybeRequestForm.has_value()) {
    close();
    return;
  }
  auto const partnerRequest = maybeRequestForm.value();

  // Validate partner public key in range.
  auto const intPubKey =
      Integer{partnerRequest.pubKey.data(), partnerRequest.pubKey.size()};
  if (!(intPubKey.Compare(2) == 1) ||
      !(intPubKey.Compare(goodPrime - 2) == -1)) {
    auto const content =
        generateFrame(compileForm(RejectForm{"Bad Public Key."}));
    _socket->write(reinterpret_cast<char const *>(content.data()),
                   static_cast<int>(content.size()));
    _socket->flush();
    close();
    return;
  }

  // Prompt user
  auto const response =
      emit promptUserIncoming(_partnerAddress, partnerRequest);
  if (!response) {
    auto const content =
        generateFrame(compileForm(RejectForm{"User declined."}));
    _socket->write(reinterpret_cast<char const *>(content.data()),
                   static_cast<int>(content.size()));
    _socket->flush();
    close();
    return;
  }
  auto const maybeUserInfo = emit promptUserOutgoing(_partnerAddress);
  if (!maybeUserInfo.has_value()) {
    auto const content =
        generateFrame(compileForm(RejectForm{"User declined."}));
    _socket->write(reinterpret_cast<char const *>(content.data()),
                   static_cast<int>(content.size()));
    _socket->flush();
    close();
    return;
  }
  auto const userInfo = maybeUserInfo.value();
  _myTime = userInfo.form.timestamp;
  _myAlias = userInfo.form.alias;
  _passHash = userInfo.passHash;

  _partnerAlias = partnerRequest.alias;
  _partnerTime = partnerRequest.timestamp;
  _partnerPubKey = partnerRequest.pubKey;

  prepareDhExchange();
  SecByteBlock secret{_dhSolver.AgreedValueLength()};
  _dhSolver.Agree(secret.data(), _myPrivKey, _partnerPubKey, false);
  establishSKey(secret);

  _authDigests = generateAuthDigests(_sKey);

  // Send Request & SecAck
  auto const requestContent =
      generateFrame(compileForm(RequestForm{_myAlias, _myTime, _myPubKey}));
  auto const secAckContent =
      generateFrame(compileForm(SecAckForm{_authDigests.second}));
  auto fullContent{requestContent};
  fullContent.insert(fullContent.end(), secAckContent.begin(),
                     secAckContent.end());
  if (!_socket->write(reinterpret_cast<char const *>(fullContent.data()),
                      static_cast<int>(fullContent.size()))) {
    emit onFailure(HandshakeFailCause::NetworkErr);
    close();
    return;
  }

  _timeout.setInterval(longTimeout);
  _state = State::RecvSecAck;
  if (_frameFilter.isReady()) {
    handleRecvSecAck();
  }
}

auto Handshaker::handleRecvSecAck() -> void {
  auto const frame = _frameFilter.pop().value();
  if (decompileRejectForm(frame).has_value()) {
    emit onFailure(HandshakeFailCause::Rejected);
    close();
    return;
  }
  auto const maybeSecAck = decompileSeckAckForm(frame);
  if (!maybeSecAck.has_value()) {
    emit onFailure(HandshakeFailCause::NetworkErr);
    close();
    return;
  }

  if (maybeSecAck.value().digest != _authDigests.first) {
    auto const rejectContent =
        generateFrame(compileForm(RejectForm{"Failed Authentication."}));
    _socket->write(reinterpret_cast<char const *>(rejectContent.data()),
                   static_cast<int>(rejectContent.size()));
    emit onFailure(HandshakeFailCause::FailedAuth);
    close();
    return;
  }
  _socket->flush();

  auto const acceptContent = generateFrame(compileForm(AcceptForm{}));
  if (!_socket->write(reinterpret_cast<char const *>(acceptContent.data()),
                      static_cast<int>(acceptContent.size()))) {
    emit onFailure(HandshakeFailCause::FailedAuth);
    close();
    return;
  }
  _socket->flush();

  // Generate chatroom
  auto room = createRoom();
  assignSubKeys(_sKey, room->_inChannel, room->_outChannel);
  emit onSuccess(room);
  close();
}

auto Handshaker::prepareDhExchange() -> void {
  AutoSeededRandomPool prng{};
  auto const generator =
      a_exp_b_mod_c(Integer{_passHash.data(), _passHash.size()}, 2, goodPrime);
  _dhSolver = DH{goodPrime, generator};
  _myPrivKey.resize(_dhSolver.PrivateKeyLength());
  _myPubKey.resize(_dhSolver.PublicKeyLength());
  _dhSolver.GenerateKeyPair(prng, _myPrivKey, _myPubKey);
}

auto Handshaker::establishSKey(SecByteBlock secret) -> void {
  auto idKey =
      generateIdKey(generateSideKey(_myAlias, secret, _myTime),
                    generateSideKey(_partnerAlias, secret, _partnerTime));
  Scrypt hasher{};
  _sKey = SecByteBlock{64};
  hasher.DeriveKey(_sKey.data(), _sKey.size(), secret.data(), secret.size(),
                   idKey.data(), idKey.size());
}

auto Handshaker::createRoom() -> Chatroom * {
  auto result = new Chatroom();
  result->_socket = _socket;
  QObject::disconnect(_socket, &QTcpSocket::disconnected, this,
                      &Handshaker::handleDisconnect);
  QObject::disconnect(_socket, &QTcpSocket::readyRead, this,
                      &Handshaker::handlePacket);
  _socket = nullptr;

  result->_frameFilter = _frameFilter;
  result->_passHash = _passHash;
  result->_inChannel =
      ChannelData{SecByteBlock{}, SecByteBlock{}, SecByteBlock{},
                  _partnerAlias,  BLAKE2b{},      0};
  result->_outChannel = ChannelData{
      SecByteBlock{}, SecByteBlock{}, SecByteBlock{}, _myAlias, BLAKE2b{}, 0};
  ;
  return result;
}
