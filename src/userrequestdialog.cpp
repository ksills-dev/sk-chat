#include <userrequestdialog.hpp>

#include <cryptopp/base64.h>
#include <cryptopp/blake2.h>
#include <cryptopp/filters.h>
#include <cryptopp/osrng.h>
using namespace CryptoPP;

#include <QGridLayout>
#include <QRegularExpressionValidator>

#include <verdigris/wobjectimpl.h>

#include <iostream>

W_OBJECT_IMPL(UserRequestDialog)

auto UserRequestDialog::ask(std::optional<QHostAddress> target)
    -> std::optional<UserRequestInfo> {
  UserRequestDialog dialog{target};
  dialog.exec();

  if (dialog._state == Result::Submitted) {
    BLAKE2b hasher{};
    SecByteBlock passHash{hasher.DigestSize()};
    auto const stupidCopy = dialog._passInput->text().toStdString();
    StringSource{stupidCopy, true,
                 new HashFilter{
                     hasher, new ArraySink{passHash.data(), passHash.size()}}};

    AutoSeededRandomPool prng;
    auto userAlias = dialog._aliasInput->text().toStdString() + "_";
    RandomNumberSource{prng, 6, true,
                       new Base64Encoder{new StringSink{userAlias}}};

    return UserRequestInfo{QHostAddress{dialog._targetInput->text()},
                           RequestForm{QString::fromStdString(userAlias),
                                       QDateTime::currentDateTime(),
                                       SecByteBlock{}},
                           passHash};
  } else {
    return std::nullopt;
  }
}

UserRequestDialog::UserRequestDialog(std::optional<QHostAddress> target) {
  _state = Result::Pending;

  _targetLabel = new QLabel{"Target IP:"};
  _targetInput = new QLineEdit{};
  _targetInput->setFixedWidth(200);
  _targetInput->setInputMask("900.900.900.900;_");
  if (target.has_value()) {
    _targetInput->setText(target.value().toString());
    _targetInput->setReadOnly(true);
    _targetInput->setValidator(new QRegExpValidator{
        QRegExp{"(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\\."
                "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\\."
                "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])\\."
                "(?:[0-1]?[0-9]?[0-9]|2[0-4][0-9]|25[0-5])"}});
  }
  _targetErrLabel = new QLabel{""};

  _aliasLabel = new QLabel{"Username:"};
  _aliasInput = new QLineEdit{};
  _aliasInput->setFixedWidth(200);
  _aliasErrLabel = new QLabel{""};

  _passLabel = new QLabel{"Password: "};
  _passInput = new QLineEdit{};
  _passErrLabel = new QLabel{""};
  _passInput->setEchoMode(QLineEdit::Password);
  _passInput->setFixedWidth(200);

  _cancelButton = new QPushButton{"Cancel"};
  _cancelButton->setAutoDefault(false);
  _cancelButton->setDefault(false);
  _connectButton = new QPushButton{"Connect"};
  _connectButton->setAutoDefault(true);
  _connectButton->setDefault(false);
  QObject::connect(_cancelButton, &QPushButton::pressed, this,
                   &UserRequestDialog::handleCancelButton);
  QObject::connect(_connectButton, &QPushButton::pressed, this,
                   &UserRequestDialog::handleConnectButton);
  QObject::connect(_targetInput, &QLineEdit::returnPressed, this,
                   &UserRequestDialog::handleConnectButton);
  QObject::connect(_aliasInput, &QLineEdit::returnPressed, this,
                   &UserRequestDialog::handleConnectButton);
  QObject::connect(_passInput, &QLineEdit::returnPressed, this,
                   &UserRequestDialog::handleConnectButton);

  auto centralLayout = new QGridLayout(this);
  centralLayout->addWidget(_targetLabel, 0, 0, Qt::AlignLeft);
  centralLayout->addWidget(_targetInput, 0, 1, Qt::AlignCenter);
  centralLayout->addWidget(_targetErrLabel, 0, 2, Qt::AlignRight);

  centralLayout->addWidget(_aliasLabel, 1, 0, Qt::AlignLeft);
  centralLayout->addWidget(_aliasInput, 1, 1, Qt::AlignCenter);
  centralLayout->addWidget(_aliasErrLabel, 1, 2, Qt::AlignRight);

  centralLayout->addWidget(_passLabel, 2, 0, Qt::AlignLeft);
  centralLayout->addWidget(_passInput, 2, 1, Qt::AlignCenter);
  centralLayout->addWidget(_passErrLabel, 2, 2, Qt::AlignRight);

  centralLayout->addWidget(_cancelButton, 3, 0, Qt::AlignCenter);
  centralLayout->addWidget(_connectButton, 3, 1, Qt::AlignCenter);

  setLayout(centralLayout);
}

auto UserRequestDialog::handleConnectButton() -> void {
  // Validate Forms
  auto success = true;
  if (!_targetInput->isReadOnly() && !_targetInput->hasAcceptableInput()) {
    success = false;
    _targetErrLabel->setText("<font color=\"#ff0d00\">Invalid IP</font>");
  }
  if (_aliasInput->text().isEmpty()) {
    success = false;
    _aliasErrLabel->setText("<font color=\"#ff0d00\">Required Field</font>");
  }
  if (_passInput->text().size() < 6) {
    success = false;
    _passErrLabel->setText("<font color=\"#ff0d00\">Too Short</font>");
  }

  if (!success) {
    return;
  }

  _state = Result::Submitted;
  close();
}

auto UserRequestDialog::handleCancelButton() -> void {
  _state = Result::Cancelled;
  close();
}
