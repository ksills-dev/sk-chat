#include <chatroom.hpp>

#include <QHostAddress>
#include <QTcpServer>

#include <cryptopp/base64.h>
#include <cryptopp/des.h>
#include <cryptopp/filters.h>
#include <cryptopp/scrypt.h>
using namespace CryptoPP;

#include <verdigris/wobjectimpl.h>

#include <chrono>
#include <cstdint>
#include <type_traits>
#include <variant>

namespace {
// Chat Timer intervals.
static std::chrono::seconds const timeoutInterval{3};
static std::chrono::seconds const keepAliveInterval{1};

// Text output for chat events.
static QString const openMark{"<p align=\"center\">"
                              "Chat started & secure (%1)<br>"
                              "〈%3〉|〈%4〉"
                              "</p><hr>"};
static QString const disconnectMark{"<p align=\"center\">"
                                    "Partner Disconnected. (%1)"
                                    "</p><br>"};
static QString const closeMark{"<p align=\"center\">"
                               "Chat has been closed. (%1)"
                               "</p><br>"};
static QString const timeoutMark{"<p align=\"center\"><font color=\"#a8a6a5\">"
                                 "Partner Timed Out."
                                 "</font></p>"
                                 "<hr><br>"};
static QString const panicMark{
    "<p align=\"center\"><font color=\"#eb120e\"><h1>"
    "***!!PANIC %1!!***"
    "</h1></font></r>"
    "<hr><br>"};
static QString const messageFormat{"%1<br><font color=\"#a8a6a5\">"
                                   "〈%3〉- %2"
                                   "</font><br>"};
static QString const tumbleMark{"<font color=\"#a8a6a5\"><sub>"
                                "🔒 Key Tumbled 〈%2〉(%1)"
                                "</sub></font>"
                                "<br>"};
static QString const outgoingHeader{"<p align=\"right\"><h1>%1</h1><br>"};
static QString const incomingHeader{"<p align=\"left\"><h1>%1</h1><br>"};
static QString const allFooter{"</p><hr><br>"};

static int const digestDisplayDigits = 6;
} // namespace

Chatroom::Chatroom()
    : _lastLogged{ActiveParty::None}, _chatLog{}, _chatCursor{&_chatLog} {}

auto Chatroom::alive() const -> bool { return !_closed; }

auto Chatroom::id() const -> ChatroomId { return _id; }

auto Chatroom::socket() const -> QTcpSocket const * { return _socket; }

auto Chatroom::chatLog() -> QTextDocument * { return &_chatLog; }

auto Chatroom::incomingChannel() const -> ChannelData const * {
  return &_inChannel;
}

auto Chatroom::outgoingChannel() const -> ChannelData const * {
  return &_outChannel;
}

auto Chatroom::passHash() const -> SecByteBlock const * { return &_passHash; }

auto Chatroom::start(ChatroomId const id) -> void {
  QObject::connect(_socket, &QTcpSocket::readyRead, this,
                   &Chatroom::handlePacket);
  QObject::connect(_socket, &QTcpSocket::disconnected, this,
                   &Chatroom::handleDisconnect);

  QObject::connect(&_timeoutClock, &QTimer::timeout, this,
                   &Chatroom::handleTimeout);
  QObject::connect(&_keepAliveClock, &QTimer::timeout, this,
                   &Chatroom::handleKeepAlive);
  _timeoutClock.setInterval(3000);
  _timeoutClock.start();
  _keepAliveClock.setInterval(1000);
  _keepAliveClock.start();

  _inChannel.runningDigest = BLAKE2b{};
  _inChannel.msgCount = 0;
  _outChannel.runningDigest = BLAKE2b{};
  _outChannel.msgCount = 0;

  this->_id = id;

  std::string inKeyString{};
  ArraySource{_inChannel.sKey.data(), _inChannel.sKey.size(), true,
              new Base64Encoder{new StringSink(inKeyString)}};
  std::string outKeyString{};
  ArraySource{_outChannel.sKey.data(), _outChannel.sKey.size(), true,
              new Base64Encoder{new StringSink(outKeyString)}};
  _chatCursor.insertHtml(
      openMark.arg(QDateTime::currentDateTime().toString())
          .arg(QString::fromStdString(inKeyString).left(digestDisplayDigits))
          .arg(QString::fromStdString(outKeyString).left(digestDisplayDigits)));
}

auto Chatroom::close() -> void {
  if (!_closed) {
    _closed = true;
    _timeoutClock.stop();
    _keepAliveClock.stop();
    if (_lastLogged != ActiveParty::None) {
      _chatCursor.insertHtml(allFooter);
      _lastLogged = ActiveParty::None;
    }
    _chatCursor.insertHtml(
        closeMark.arg(QDateTime::currentDateTime().toString()));

    emit onClose(_id);

    _socket->disconnect();
    _socket->close();
    _socket->deleteLater();
    _socket = nullptr;
    disconnect();
  }
}

auto Chatroom::sendMessage(QString body) -> void {
  _keepAliveClock.start();
  if (_lastLogged != ActiveParty::Outgoing) {
    _lastLogged = ActiveParty::Outgoing;
    _chatCursor.insertHtml(allFooter);
    _chatCursor.insertHtml(outgoingHeader.arg(_outChannel.alias.chopped(8)));
  }

  MessageForm form;
  form.body = body;
  form.timestamp = QDateTime::currentDateTime();

  // This process is pretty inefficient.
  // The reason it's currently done this way is due to the original hacky
  // design - the form compiler handled generation of the AuthCode field.
  // As a quick implementation of hash displays, this allows us to extract it.
  auto const outgoingForm =
      compileForm(form, _outChannel.mKey, _outChannel.aKey);
  auto const filledForm = std::get<MessageForm>(
      decompileChatForm(outgoingForm, _outChannel.mKey, _outChannel.aKey)
          .value());
  auto const outgoing = generateFrame(outgoingForm);
  _socket->write(reinterpret_cast<char const *>(outgoing.data()),
                 static_cast<std::int64_t>(outgoing.size()));
  _socket->flush();

  std::string authString{};
  ArraySource{filledForm.auth.data(), filledForm.auth.size(), true,
              new Base64Encoder(new StringSink{authString}, false)};
  _chatCursor.insertHtml(
      messageFormat.arg(body)
          .arg(form.timestamp.toLocalTime().toString())
          .arg(QString::fromStdString(authString).left(digestDisplayDigits)));

  _outChannel.runningDigest.Update(
      reinterpret_cast<std::uint8_t const *>(body.data()),
      static_cast<std::uint64_t>(body.size()));
  if (++_outChannel.msgCount == tumbleInterval) {
    keyTumble(_outChannel);
  }
}

auto Chatroom::handlePacket() -> void {
  auto const baContent = _socket->readAll();
  std::vector<std::uint8_t> content{baContent.begin(), baContent.end()};
  _frameFilter.push(content);
  while (!_closed && _frameFilter.isReady()) {
    std::invoke_result<decltype(decompileChatForm),
                       std::vector<std::uint8_t> const &, SecByteBlock const &,
                       SecByteBlock const &>::type form;
    try {
      form = decompileChatForm(_frameFilter.pop().value(), _inChannel.mKey,
                               _inChannel.aKey);
    } catch (HashVerificationFilter::HashVerificationFailed) {
      panic();
      return;
    }

    if (form.has_value()) {
      std::visit(
          [=](auto &&form) {
            using T = std::decay_t<decltype(form)>;
            if constexpr (std::is_same_v<T, MessageForm>) {
              _timeoutClock.start();
              if (_lastLogged != ActiveParty::Incoming) {
                _lastLogged = ActiveParty::Incoming;
                _chatCursor.insertHtml(allFooter);
                _chatCursor.insertHtml(
                    incomingHeader.arg(_inChannel.alias.chopped(8)));
              }

              std::string authString{};
              ArraySource{form.auth.data(), form.auth.size(), true,
                          new Base64Encoder(new StringSink{authString}, false)};
              _chatCursor.insertHtml(messageFormat.arg(form.body)
                                         .arg(form.timestamp.toString())
                                         .arg(QString::fromStdString(authString)
                                                  .left(digestDisplayDigits)));

              _inChannel.runningDigest.Update(
                  reinterpret_cast<std::uint8_t const *>(form.body.data()),
                  static_cast<std::uint64_t>(form.body.size()));
              if (++_inChannel.msgCount == tumbleInterval) {
                keyTumble(_inChannel);
              }
              emit onMessageReceived(_id, form);
            } else if constexpr (std::is_same_v<T, PanicForm>) {
              handlePanic();
            } else {
              _timeoutClock.start();
            }
          },
          form.value());
    } else {
      panic();
      return;
    }
  }
}

auto Chatroom::handleTimeout() -> void {
  if (_lastLogged != ActiveParty::None) {
    _chatCursor.insertHtml(allFooter);
    _lastLogged = ActiveParty::None;
  }
  _chatCursor.insertHtml(timeoutMark);
  close();
}

auto Chatroom::handleKeepAlive() -> void {
  auto const outgoing =
      generateFrame(compileForm(KeepAliveForm{}, _outChannel.mKey));
  _socket->write(reinterpret_cast<char const *>(outgoing.data()),
                 static_cast<std::int64_t>(outgoing.size()));
  _keepAliveClock.start();
}

auto Chatroom::keyTumble(ChannelData &channel) -> void {
  // Set up a secret for hashing (H(Password) + MessageDigest)
  SecByteBlock fullSecret{_passHash};
  SecByteBlock rDigest{channel.runningDigest.DigestSize()};
  channel.runningDigest.Final(rDigest.data());
  fullSecret += rDigest;

  // The running digest must include entropy from the entire conversation.
  // CryptoPP, however, provides no means of taking a hash without resetting
  // hasher state.
  channel.runningDigest.Update(rDigest.data(), rDigest.size());

  // Use strong hash with full session key as salt.
  Scrypt tumbler{};
  SecByteBlock newSKey{channel.sKey.size()};
  tumbler.DeriveKey(newSKey.data(), newSKey.size(), fullSecret.data(),
                    fullSecret.size(), channel.sKey.data(),
                    channel.sKey.size());
  channel.sKey = newSKey;
  channel.mKey.Assign(newSKey.data(), DES::KEYLENGTH);
  channel.aKey.Assign(newSKey.data() + DES::KEYLENGTH, DES::KEYLENGTH);

  std::string sessionKeyString{};
  ArraySource{channel.sKey.data(), channel.sKey.size(), true,
              new Base64Encoder{new StringSink{sessionKeyString}}};
  _chatCursor.insertHtml(tumbleMark.arg(QDateTime::currentDateTime().toString())
                             .arg(QString::fromStdString(sessionKeyString)
                                      .left(digestDisplayDigits)));

  channel.msgCount = 0;
  emit onTumble(_id);
}

auto Chatroom::handlePanic() -> void {
  if (_lastLogged != ActiveParty::None) {
    _chatCursor.insertHtml(allFooter);
    _lastLogged = ActiveParty::None;
  }
  _chatCursor.insertHtml(panicMark.arg("RECEIVED"));

  emit onPanic(_id);
  close();
}

auto Chatroom::handleDisconnect() -> void {
  if (_lastLogged != ActiveParty::None) {
    _chatCursor.insertHtml(allFooter);
    _lastLogged = ActiveParty::None;
  }
  _chatCursor.insertHtml(
      disconnectMark.arg(QDateTime::currentDateTime().toString()));

  emit onDisconnect(_id);
  close();
}

auto Chatroom::panic() -> void {
  if (_lastLogged != ActiveParty::None) {
    _chatCursor.insertHtml(allFooter);
    _lastLogged = ActiveParty::None;
  }
  _chatCursor.insertHtml(panicMark.arg("SENT"));

  emit onPanic(_id);
  close();
}

W_OBJECT_IMPL(Chatroom)
