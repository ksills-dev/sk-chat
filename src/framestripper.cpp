#include <framestripper.hpp>

#include <QtEndian>

#include <limits>
#include <stdexcept>

auto generateFrame(const std::vector<uint8_t> &content)
    -> std::vector<std::uint8_t> {
  if (content.size() > std::numeric_limits<std::uint16_t>::max()) {
    throw std::range_error("Frame length limited to 16-bit prefix.");
  }
  auto const beSize = qToBigEndian(static_cast<std::uint16_t>(content.size()));

  std::vector<std::uint8_t> frame{};
  frame.push_back(reinterpret_cast<std::uint8_t const *>(&beSize)[0]);
  frame.push_back(reinterpret_cast<std::uint8_t const *>(&beSize)[1]);
  std::copy(content.begin(), content.end(), std::back_inserter(frame));
  return frame;
}

// FrameStripper implementations.

auto FrameStripper::bufferRegion() -> std::pair<std::uint8_t *, std::size_t> {
  return std::make_pair(_buffer.data(), _buffer.size());
}

auto FrameStripper::bufferRegion() const
    -> std::pair<std::uint8_t const *, std::size_t> {
  return std::make_pair(_buffer.data(), _buffer.size());
}

auto FrameStripper::bufferRegion(std::size_t capacity) -> std::uint8_t * {
  if (_buffer.max_size() - _buffer.size() < capacity) {
    _buffer.resize(_buffer.size() + capacity);
  }

  return _buffer.data();
}

auto FrameStripper::isReady() const -> bool {
  return (_working && (_messageLength <= _buffer.size()));
}

auto FrameStripper::push(std::vector<std::uint8_t> const &input) -> void {
  if (input.size() == 0) {
    return;
  }

  auto startIter = input.begin();
  if (!_working) {
    std::uint16_t beSize{0};
    if (!_buffer.empty()) {
      reinterpret_cast<std::uint8_t *>(&beSize)[0] = _buffer.back();
      _buffer.pop_back();
      reinterpret_cast<std::uint8_t *>(&beSize)[1] = input[0];
      startIter += 1;

      _messageLength = qFromBigEndian<std::uint16_t>(beSize);
      _working = true;
    } else {
      if (input.size() < 2) {
        _buffer = input;
        return;
      }
      reinterpret_cast<std::uint8_t *>(&beSize)[0] = input[0];
      reinterpret_cast<std::uint8_t *>(&beSize)[1] = input[1];
      startIter += 2;

      _messageLength = qFromBigEndian<std::uint16_t>(beSize);
      _working = true;
    }

    if (_messageLength == 0) {
      throw std::range_error("Encountered frame length prefix of zero.");
    }
  }

  std::copy(startIter, input.end(), std::back_inserter(_buffer));
}

auto FrameStripper::pop() -> std::optional<std::vector<std::uint8_t>> {
  if (!isReady() || (_messageLength > _buffer.size())) {
    return std::nullopt;
  }

  std::vector<std::uint8_t> result{};
  result.resize(_messageLength);
  std::copy(_buffer.begin(), _buffer.begin() + _messageLength, result.begin());
  _buffer.erase(_buffer.begin(), _buffer.begin() + _messageLength);

  _messageLength = 0;
  _working = false;

  auto tmp = _buffer;
  _buffer.clear();
  push(tmp);

  return result;
}
