#include <skchatapp.hpp>

#include <forms.hpp>

#include <QAction>
#include <QDateTime>
#include <QHBoxLayout>
#include <QHostAddress>
#include <QKeySequence>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QNetworkDatagram>
#include <QRegularExpression>
#include <QScrollBar>
#include <QShortcut>
#include <QString>
#include <QTcpSocket>
#include <QTextFormat>
#include <QThread>
#include <QVBoxLayout>
#include <QWidget>

#include <cryptopp/base64.h>
#include <cryptopp/filters.h>
#include <cryptopp/integer.h>
#include <cryptopp/secblock.h>
using namespace CryptoPP;

#include <verdigris/wobjectimpl.h>

#include <optional>
#include <string>

W_OBJECT_IMPL(SkChatApp)

// Stuff only we care about.
namespace {
static std::uint16_t const defaultDispatchPort = 1337;

static QString const aboutDialogText =
    "<p align=\"center\">"
    "SkChat is a student project to develop a secure P2P chat application.<br>"
    "No guarantees are made about the quality or functionality of this "
    "product, use at your own peril!"
    "<hr>"
    "Authors: {Kenneth Sills, Rajeshwar Gandi, Eduardo Gonzalez}<br>"
    "MIT License - Copyright 2018"
    "</p>";

static QString const dateTimeFormat = "d.M.yy-H:m:s.z@t";

template <typename T> auto toBase64QString(T const &data) -> QString {
  std::string out{};
  ArraySource{data.data(), data.size(), true,
              new Base64Encoder{new StringSink{out}}};
  return QString::fromStdString(out);
}
} // namespace

SkChatApp::SkChatApp(int &argc, char **argv)
    : QApplication(argc, argv), _window{}, _dispatch{}, _activeRooms{} {
  {
    qRegisterMetaType<HandshakeFailCause>("HandshakeFailCause");

    // Register actions on menu items.
    auto fileMenu = _window.menuBar()->addMenu("File");
    fileMenu->addAction("Exit", [=] {
      if (_activeRooms.empty() ||
          QMessageBox::question(
              &_window, "Confirm Chat Close",
              "Are you sure you'd like to close all active chats?") ==
              QMessageBox::Yes) {
        close();
      }
    });
    auto chatMenu = _window.menuBar()->addMenu("Chat");
    chatMenu->addAction("Request New Chat",
                        [=] {
                          auto const userResponse = promptUserOutgoingChat();
                          if (userResponse.has_value()) {
                            startInitiatorHandshake(userResponse.value());
                          }
                        },
                        QKeySequence{"Ctrl+n"});
    chatMenu->addAction("Close Chat",
                        [=] { closeTab(_chatSelection->currentIndex()); },
                        QKeySequence{"Ctrl+w"});
    auto helpMenu = _window.menuBar()->addMenu("Help");
    helpMenu->addAction("About", [=] {
      QMessageBox::information(&_window, "About SkChat", aboutDialogText);
    });
    helpMenu->addAction("About Qt", [=] { QMessageBox::aboutQt(&_window); });
  }

  _messageViewer = new QTextEdit{};
  _messageViewer->setReadOnly(true);
  _messageViewer->setPlaceholderText("Find some friends, nerd.");

  _messageEntry = new QLineEdit{};
  _messageEntry->setPlaceholderText("Enter Message...");
  _messageEntry->setMaxLength(32768);
  QObject::connect(new QShortcut{QKeySequence{"Ctrl+b"}, _messageEntry},
                   &QShortcut::activated, this, &SkChatApp::boldifyEntry);
  QObject::connect(new QShortcut{QKeySequence{"Ctrl+i"}, _messageEntry},
                   &QShortcut::activated, this, &SkChatApp::italicizeEntry);
  QObject::connect(new QShortcut{QKeySequence{"Ctrl+u"}, _messageEntry},
                   &QShortcut::activated, this, &SkChatApp::underlineEntry);

  _sendButton = new QPushButton{"Send"};
  QObject::connect(_sendButton, &QPushButton::pressed, this,
                   &SkChatApp::trySendMessage);

  _statsView = new QTextEdit{};
  _statsView->setReadOnly(true);
  _statsView->setPlaceholderText("No Open Chat.");

  _chatSelection = new QTabBar{};
  _chatSelection->setExpanding(false);
  _chatSelection->setTabsClosable(true);
  QObject::connect(_chatSelection, &QTabBar::currentChanged, this,
                   &SkChatApp::handleTabChanged);
  QObject::connect(_chatSelection, &QTabBar::tabCloseRequested, this,
                   &SkChatApp::closeTab);
  QObject::connect(
      new QShortcut{QKeySequence{"Ctrl+Tab"}, _chatSelection},
      &QShortcut::activated, [=]() {
        if (!_activeRooms.empty()) {
          _chatSelection->setCurrentIndex((_chatSelection->currentIndex() + 1) %
                                          _chatSelection->count());
          changeFocus(_activeRooms[_chatSelection->currentIndex()]);
        }
      });

  setPlaceholders();

  auto messageEntryLayout = new QHBoxLayout{};
  messageEntryLayout->addWidget(_messageEntry);
  messageEntryLayout->addWidget(_sendButton);
  auto messageViewLayout = new QVBoxLayout{};
  messageViewLayout->addWidget(_messageViewer);
  messageViewLayout->addLayout(messageEntryLayout);
  auto chatViewLayout = new QHBoxLayout{};
  chatViewLayout->addLayout(messageViewLayout, 2);
  chatViewLayout->addWidget(_statsView);
  auto tabberLayout = new QVBoxLayout{};
  tabberLayout->addWidget(_chatSelection);
  tabberLayout->addLayout(chatViewLayout);

  auto const layoutWidget = new QWidget{};
  layoutWidget->setLayout(tabberLayout);

  // Set up Signals & Slots
  QObject::connect(&_dispatch, &QTcpServer::newConnection, this,
                   &SkChatApp::handleIncomingRequest);
  _dispatch.listen(QHostAddress::Any, defaultDispatchPort);

  QObject::connect(_messageEntry, &QLineEdit::returnPressed, _sendButton,
                   &QPushButton::click);

  _window.setCentralWidget(layoutWidget);
  _window.show();
}

auto SkChatApp::close() -> void {
  _dispatch.close();
  for (auto &room : _activeRooms) {
    room->close();
    delete room;
  }

  emit onClose();
  _window.close();
}

auto SkChatApp::boldifyEntry() -> void {
  auto const textBase = _messageEntry->selectedText();
  _messageEntry->insert("<b>" + textBase);
  auto const insertPoint = _messageEntry->cursorPosition();
  _messageEntry->insert("</b>");
  if (textBase.isEmpty()) {
    _messageEntry->setCursorPosition(insertPoint);
  }
}

auto SkChatApp::italicizeEntry() -> void {
  auto const textBase = _messageEntry->selectedText();
  _messageEntry->insert("<i>" + textBase);
  auto const insertPoint = _messageEntry->cursorPosition();
  _messageEntry->insert("</i>");
  if (textBase.isEmpty()) {
    _messageEntry->setCursorPosition(insertPoint);
  }
}

auto SkChatApp::underlineEntry() -> void {
  auto const textBase = _messageEntry->selectedText();
  _messageEntry->insert("<u>" + textBase);
  auto const insertPoint = _messageEntry->cursorPosition();
  _messageEntry->insert("</u>");
  if (textBase.isEmpty()) {
    _messageEntry->setCursorPosition(insertPoint);
  }
}

auto SkChatApp::monospaceEntry() -> void {
  auto const textBase = _messageEntry->selectedText();
  _messageEntry->insert("<tt>" + textBase);
  auto const insertPoint = _messageEntry->cursorPosition();
  _messageEntry->insert("</tt>");
  if (textBase.isEmpty()) {
    _messageEntry->setCursorPosition(insertPoint);
  }
}

auto SkChatApp::handleIncomingRequest() -> void {
  auto socket = _dispatch.nextPendingConnection();
  startReceiverHandshake(socket);
}

auto SkChatApp::handleGoodHandshake(Chatroom *newRoom) -> void {
  QObject::connect(newRoom, &Chatroom::onMessageReceived, this,
                   &SkChatApp::handleMessage);
  QObject::connect(newRoom, &Chatroom::onPanic, this, &SkChatApp::handlePanic);
  QObject::connect(newRoom, &Chatroom::onDisconnect, this,
                   &SkChatApp::handleDisconnect);
  QObject::connect(newRoom, &Chatroom::onClose, this, &SkChatApp::handleClose);

  _activeRooms.push_back(newRoom);
  newRoom->start(_uidFactory.generateId());

  _chatSelection->addTab(newRoom->incomingChannel()->alias.chopped(10));

  if (_activeRooms.size() == 1) {
    removePlaceholders();
  }

  QMessageBox::information(
      &_window, "Secure Chat Created!",
      "Secure connection with partner has been established. Chat away!");
}

auto SkChatApp::handleBadHandshake(HandshakeFailCause cause) -> void {
  QString text;
  switch (cause) {
  case HandshakeFailCause::NetworkErr:
    text = "Could not establish reliable connection.";
    break;
  case HandshakeFailCause::FailedAuth:
    text = "Could not authenticate passwords.";
    break;
  case HandshakeFailCause::Rejected:
    text = "Partner refused chat request.";
    break;
  }
  QMessageBox::warning(&_window, "Chat Connection Failed!", text);
}

auto SkChatApp::handleMessage(ChatroomId id) -> void {
  auto const i =
      std::find_if(_activeRooms.begin(), _activeRooms.end(),
                   [=](auto const &room) { return (room->id() == id); }) -
      _activeRooms.begin();
  if (i != _chatSelection->currentIndex()) {
    _chatSelection->setTabText(
        i, _activeRooms[i]->incomingChannel()->alias.chopped(10) + " 🔔");
  } else {
    _messageViewer->verticalScrollBar()->triggerAction(
        QScrollBar::SliderToMaximum);
    updateStats(_activeRooms[i]);
  }
}

auto SkChatApp::handlePanic(ChatroomId id) -> void {
  auto const i =
      std::find_if(_activeRooms.begin(), _activeRooms.end(),
                   [=](auto const &room) { return (room->id() == id); }) -
      _activeRooms.begin();
  QMessageBox::critical(
      &_window, QString{"CHATROOM PANIC!"},
      QString{
          "The chatroom with %1 has encountered a "
          "<font color=\"red\">fatal error</font> and has been shut down!<br>"
          "Reopen the chat at your own discretion. It is recommended to"
          "change the password, use new usernames, and watch out for any"
          "old chat requests."}
          .arg(_activeRooms[i]->incomingChannel()->alias.chopped(10)));
  _chatSelection->setTabText(i, _chatSelection->tabText(i) + " ☠");
}

auto SkChatApp::handleDisconnect(ChatroomId id) -> void {
  auto const i =
      std::find_if(_activeRooms.begin(), _activeRooms.end(),
                   [=](auto const &room) { return (room->id() == id); }) -
      _activeRooms.begin();
  _chatSelection->setTabText(i, _chatSelection->tabText(i) + " ⎊");
}

auto SkChatApp::handleClose(ChatroomId id) -> void {
  auto const i =
      std::find_if(_activeRooms.begin(), _activeRooms.end(),
                   [=](auto const &room) { return (room->id() == id); }) -
      _activeRooms.begin();
  if (i == _chatSelection->currentIndex()) {
    _messageEntry->setReadOnly(true);
    _sendButton->setDisabled(true);
    _messageViewer->verticalScrollBar()->triggerAction(
        QScrollBar::SliderToMaximum);
    updateStats(_activeRooms[i]);
  }
}

auto SkChatApp::handleTabChanged(int index) -> void {
  if (index == -1) {
    setPlaceholders();
  } else if (!_activeRooms.empty()) {
    changeFocus(_activeRooms[index]);
  }
}

auto SkChatApp::closeTab(int index) -> void {
  if (_activeRooms.empty()) {
    return;
  }

  if (QMessageBox::question(&_window, "Confirm Chat Close",
                            "Are you sure you would like to close the "
                            "active chat window?") == QMessageBox::Yes) {
    if (index != _chatSelection->currentIndex()) {
      changeFocus(_activeRooms[index]);
    }
    _activeRooms[index]->close();
    _uidFactory.freeId(_activeRooms[index]->id());
    _activeRooms[index]->deleteLater();
    _activeRooms.erase(_activeRooms.begin() + index);

    _chatSelection->removeTab(_chatSelection->currentIndex());
  }
}

auto SkChatApp::trySendMessage() -> void {
  auto const body = _messageEntry->text()
                        .remove(QRegularExpression{"</?h[0-9]>"})
                        .remove(QRegularExpression{"</?head>"})
                        .remove("<hr>");
  if (!body.isEmpty()) {
    _activeRooms[_chatSelection->currentIndex()]->sendMessage(body);
    _messageEntry->clear();
  }
  _messageViewer->ensureCursorVisible();
  updateStats(_activeRooms[_chatSelection->currentIndex()]);
}

auto SkChatApp::promptUserIncomingChat(QHostAddress const &from,
                                       RequestForm const &incoming) -> bool {
  auto confirmDialog =
      new QMessageBox{QMessageBox::Question, "Incoming Chat Request",
                      QString{"<h1>Accept this Chat Request?</h1>"
                              "<table>"
                              "<tr><td><b>Username:</b> <td>%1"
                              "<tr><td><b>IP:</b>       <td>%2"
                              "<tr><td><b>Time:</b>     <td>%3"
                              "</table>"}
                          .arg(incoming.alias.chopped(10))
                          .arg(from.toString())
                          .arg(incoming.timestamp.toLocalTime().toString()),
                      QMessageBox::Yes | QMessageBox::No, &_window};
  auto userResponse = confirmDialog->exec();
  return (userResponse == QMessageBox::Yes);
}

auto SkChatApp::promptUserOutgoingChat(std::optional<QHostAddress> target)
    -> std::optional<UserRequestInfo> {
  return UserRequestDialog::ask(target);
}

auto SkChatApp::startInitiatorHandshake(UserRequestInfo userData) -> void {
  auto shaker = new Handshaker{userData};
  QObject::connect(this, &SkChatApp::onClose, shaker, &Handshaker::close);
  QObject::connect(shaker, &Handshaker::promptUserIncoming, this,
                   &SkChatApp::promptUserIncomingChat);
  QObject::connect(shaker, &Handshaker::promptUserOutgoing, this,
                   &SkChatApp::promptUserOutgoingChat);
  QObject::connect(shaker, &Handshaker::onSuccess, this,
                   &SkChatApp::handleGoodHandshake, Qt::QueuedConnection);
  QObject::connect(shaker, &Handshaker::onFailure, this,
                   &SkChatApp::handleBadHandshake, Qt::QueuedConnection);
}

auto SkChatApp::startReceiverHandshake(QTcpSocket *socket) -> void {
  auto shaker = new Handshaker{socket};
  QObject::connect(this, &SkChatApp::onClose, shaker, &Handshaker::close);
  QObject::connect(shaker, &Handshaker::promptUserIncoming, this,
                   &SkChatApp::promptUserIncomingChat);
  QObject::connect(shaker, &Handshaker::promptUserOutgoing, this,
                   &SkChatApp::promptUserOutgoingChat);
  QObject::connect(shaker, &Handshaker::onSuccess, this,
                   &SkChatApp::handleGoodHandshake, Qt::QueuedConnection);
  QObject::connect(shaker, &Handshaker::onFailure, this,
                   &SkChatApp::handleBadHandshake, Qt::QueuedConnection);
}

auto SkChatApp::setPlaceholders() -> void {
  _messageEntry->clear();
  _messageEntry->setReadOnly(true);
  _sendButton->setDisabled(true);
  _chatSelection->addTab("No Active Chats...");
  _messageViewer->setDocument(new QTextDocument{_messageViewer});
  _statsView->setDocument(new QTextDocument{_messageViewer});
}

auto SkChatApp::removePlaceholders() -> void { _chatSelection->removeTab(0); }

auto SkChatApp::changeFocus(Chatroom *room) -> void {
  _messageViewer->setDocument(room->chatLog());
  _messageEntry->clear();
  if (!room->alive()) {
    _chatSelection->setTabText(_chatSelection->currentIndex(),
                               room->incomingChannel()->alias.chopped(10) +
                                   " ⎊");
    _messageEntry->setReadOnly(true);
    _sendButton->setDisabled(true);
  } else {
    _chatSelection->setTabText(_chatSelection->currentIndex(),
                               room->incomingChannel()->alias.chopped(10));
    _messageEntry->setReadOnly(false);
    _sendButton->setDisabled(false);
  }
  updateStats(room);
}

auto SkChatApp::updateStats(Chatroom *room) -> void {
  if (room != nullptr) {
    // This would be much more efficient if implemented as a QTableWidget.
    // But that would be slightly more effort.

    auto const passHash = toBase64QString(*room->passHash());
    auto const roomId = QString::number(room->id().raw());

    auto const userChannel = room->outgoingChannel();
    auto const userAlias = userChannel->alias;
    QString userPort{};
    if (room->socket() != nullptr) {
      userPort = QString::number(room->socket()->localPort());
    }
    auto const userSession = toBase64QString(userChannel->sKey);
    auto const userMaster = toBase64QString(userChannel->mKey);
    auto const userAuxiliary = toBase64QString(userChannel->aKey);
    auto const userKeyTtl = QString::number(
        static_cast<int>(tumbleInterval - userChannel->msgCount));

    auto const partnerChannel = room->incomingChannel();
    auto const partnerAlias = partnerChannel->alias;
    QString partnerPort{};
    if (room->socket() != nullptr) {
      partnerPort = QString::number(room->socket()->peerPort());
    }
    auto const partnerSession = toBase64QString(partnerChannel->sKey);
    auto const partnerMaster = toBase64QString(partnerChannel->mKey);
    auto const partnerAuxiliary = toBase64QString(partnerChannel->aKey);
    auto const partnerKeyTtl = QString::number(
        static_cast<int>(tumbleInterval - partnerChannel->msgCount));

    _statsView->setHtml(QString{"<table>"
                                "<tr><td colspan=\"2\"><h2>Chatroom</h2>"
                                "<tr><td><b>Password Hash:</b> <td>%1"
                                "<tr><td><b>Room ID:</b> <td>%2"
                                "<tr>"
                                "<tr><td colspan=\"2\"><h2>User (Out)</h2>"
                                "<tr><td><b>Username:</b> <td>%3"
                                "<tr><td><b>Port:</b> <td>%4"
                                "<tr><td><b>Session Key:</b> <td>%5"
                                "<tr><td><b>Master Key:</b> <td>%6"
                                "<tr><td><b>Auxiliary Key:</b> <td>%7"
                                "<tr><td><b>Key TTL:</b> <td>%8"
                                "<tr>"
                                "<tr><td colspan=\"2\"><h2>Partner (In)</h2>"
                                "<tr><td><b>Username:</b> <td>%9"
                                "<tr><td><b>Port:</b> <td>%10"
                                "<tr><td><b>Session Key:</b> <td>%11"
                                "<tr><td><b>Master Key:</b> <td>%12"
                                "<tr><td><b>Auxiliary Key:</b> <td>%13"
                                "<tr><td><b>Key TTL:</b> <td>%14"
                                "</table>"}
                            .arg(passHash, roomId)
                            .arg(userAlias, userPort, userSession, userMaster,
                                 userAuxiliary, userKeyTtl)
                            .arg(partnerAlias, partnerPort, partnerSession,
                                 partnerMaster, partnerAuxiliary,
                                 partnerKeyTtl));
  }
}
