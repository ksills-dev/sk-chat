#pragma once

#include <QDateTime>
#include <QString>

#include <cryptopp/cryptlib.h>
#include <cryptopp/integer.h>
#include <cryptopp/secblock.h>

#include <array>
#include <cstdint>
#include <optional>
#include <variant>
#include <vector>

struct RequestForm {
  QString alias;
  QDateTime timestamp;
  CryptoPP::SecByteBlock pubKey;
};

struct AcceptForm {};

struct RejectForm {
  QString reason;
};

struct SecAckForm {
  CryptoPP::SecByteBlock digest;
};

struct MessageForm {
  QString body;
  QDateTime timestamp;
  std::array<std::uint8_t, 64> auth;
};

struct PanicForm {};

struct KeepAliveForm {};

auto compileForm(RequestForm form) -> std::vector<std::uint8_t>;
auto compileForm(RejectForm form) -> std::vector<std::uint8_t>;
auto compileForm(AcceptForm form) -> std::vector<std::uint8_t>;
auto compileForm(SecAckForm form) -> std::vector<std::uint8_t>;
auto compileForm(MessageForm form, CryptoPP::SecByteBlock const &mKey,
                 CryptoPP::SecByteBlock const &aKey)
    -> std::vector<std::uint8_t>;
auto compileForm(PanicForm form, CryptoPP::SecByteBlock const &mKey)
    -> std::vector<std::uint8_t>;
auto compileForm(KeepAliveForm form, CryptoPP::SecByteBlock const &mKey)
    -> std::vector<std::uint8_t>;

auto decompileRequestForm(std::vector<std::uint8_t> content)
    -> std::optional<RequestForm>;
auto decompileRejectForm(std::vector<std::uint8_t> content)
    -> std::optional<RejectForm>;
auto decompileAcceptForm(std::vector<std::uint8_t> content)
    -> std::optional<AcceptForm>;
// For response to request
auto decompileResponseForm(std::vector<std::uint8_t> content)
    -> std::optional<std::variant<RequestForm, RejectForm>>;
auto decompileSeckAckForm(std::vector<std::uint8_t> content)
    -> std::optional<SecAckForm>;
// For final SecAck Answer
auto decompileAckResponseForm(std::vector<std::uint8_t> content)
    -> std::optional<std::variant<AcceptForm, RejectForm>>;
auto decompileChatForm(std::vector<std::uint8_t> content,
                       CryptoPP::SecByteBlock const &mKey,
                       CryptoPP::SecByteBlock const &aKey)
    -> std::optional<std::variant<MessageForm, PanicForm, KeepAliveForm>>;
