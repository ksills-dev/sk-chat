#pragma once

#include <chatroom.hpp>
#include <forms.hpp>
#include <handshaker.hpp>
#include <uidfactory.hpp>
#include <userrequestdialog.hpp>

#include <QApplication>
#include <QHostAddress>
#include <QLineEdit>
#include <QMainWindow>
#include <QObject>
#include <QPushButton>
#include <QTabBar>
#include <QTcpServer>
#include <QTextEdit>
#include <QThreadPool>

#include <verdigris/wobjectdefs.h>

#include <optional>

class SkChatApp : public QApplication {
  W_OBJECT(SkChatApp)

public:
  SkChatApp() = delete;
  SkChatApp(int &argc, char **argv);
  SkChatApp(SkChatApp &) = delete;
  auto operator=(SkChatApp &) -> SkChatApp & = delete;
  ~SkChatApp() = default;

  // Slots
  auto handleIncomingRequest() -> void;

  auto handleGoodHandshake(Chatroom *newRoom) -> void;
  auto handleBadHandshake(HandshakeFailCause cause) -> void;

  auto handleMessage(ChatroomId port) -> void;
  auto handlePanic(ChatroomId port) -> void;
  auto handleDisconnect(ChatroomId port) -> void;
  auto handleClose(ChatroomId port) -> void;

  auto handleTabChanged(int index) -> void;
  auto closeTab(int index) -> void;

private:
  auto close() -> void;
  auto onClose() -> void W_SIGNAL(onClose);

  auto boldifyEntry() -> void;
  auto italicizeEntry() -> void;
  auto underlineEntry() -> void;
  auto monospaceEntry() -> void;

  auto promptUserIncomingChat(QHostAddress const &from,
                              RequestForm const &incoming) -> bool;
  auto promptUserOutgoingChat(std::optional<QHostAddress> target = std::nullopt)
      -> std::optional<UserRequestInfo>;

  auto trySendMessage() -> void;

  auto startInitiatorHandshake(UserRequestInfo userData) -> void;
  auto startReceiverHandshake(QTcpSocket *socket) -> void;

  auto setPlaceholders() -> void;
  auto removePlaceholders() -> void;

  auto changeFocus(Chatroom *room) -> void;
  auto updateStats(Chatroom *room) -> void;

  QMainWindow _window;
  QTextEdit *_messageViewer;
  QLineEdit *_messageEntry;
  QPushButton *_sendButton;
  QTextEdit *_statsView;
  QTabBar *_chatSelection;

  QTcpServer _dispatch;
  util::UidFactory<ChatroomId> _uidFactory;

  QThreadPool _handshakeSpace;
  std::vector<Chatroom *> _activeRooms;
  std::vector<Chatroom *> _pendingRooms;
};
