#pragma once

#include <chatroom.hpp>
#include <forms.hpp>
#include <framestripper.hpp>

#include <QDateTime>
#include <QHostAddress>
#include <QObject>
#include <QTcpSocket>
#include <QTimer>

#include <verdigris/wobjectdefs.h>

#include <cryptopp/dh.h>
#include <cryptopp/secblock.h>

#include <cstdint>

enum class HandshakeFailCause { NetworkErr, FailedAuth, Rejected };

W_REGISTER_ARGTYPE(HandshakeFailCause)
W_REGISTER_ARGTYPE(Chatroom *)
W_REGISTER_ARGTYPE(QHostAddress)
W_REGISTER_ARGTYPE(RequestForm)
W_REGISTER_ARGTYPE(std::optional<QHostAddress>)
W_REGISTER_ARGTYPE(std::optional<UserRequestInfo>)

class Handshaker : public QObject {
  W_OBJECT(Handshaker)

public:
  Handshaker() = delete;
  Handshaker(UserRequestInfo userInfo); // Initiator
  Handshaker(QTcpSocket *socket);       // Receiver
  Handshaker(Handshaker const &) = delete;
  auto operator=(Handshaker const &) -> Handshaker & = delete;
  ~Handshaker() = default;

  auto close() -> void;

  auto onSuccess(Chatroom *room) -> void W_SIGNAL(onSuccess, room);
  auto onFailure(HandshakeFailCause cause) -> void W_SIGNAL(onFailure, cause);

  auto promptUserIncoming(QHostAddress from, RequestForm incoming)
      -> bool W_SIGNAL(promptUserIncoming, from, incoming);
  auto promptUserOutgoing(std::optional<QHostAddress> target)
      -> std::optional<UserRequestInfo> W_SIGNAL(promptUserOutgoing, target);

private:
  enum class State {
    InitRequest,
    InitSecAck,
    InitAuth,
    RecvRequest,
    RecvSecAck
  };

  auto handlePacket() -> void;
  auto handleDisconnect() -> void;
  auto handleTimeout() -> void;

  auto sendInitRequest() -> void;
  auto handleInitRequest() -> void;
  auto handleInitSecAck() -> void;
  auto handleInitAuth() -> void;
  auto handleRecvRequest() -> void;
  auto handleRecvSecAck() -> void;

  auto prepareDhExchange() -> void;
  auto establishSKey(CryptoPP::SecByteBlock secret) -> void;
  auto createRoom() -> Chatroom *;

  bool _closed = false;

  QHostAddress _partnerAddress;
  QTcpSocket *_socket;

  QDateTime _myTime;
  QString _myAlias;
  QDateTime _partnerTime;
  QString _partnerAlias;

  CryptoPP::SecByteBlock _passHash;

  CryptoPP::DH _dhSolver;
  CryptoPP::SecByteBlock _myPubKey;
  CryptoPP::SecByteBlock _myPrivKey;
  CryptoPP::SecByteBlock _partnerPubKey;
  CryptoPP::SecByteBlock _sKey;
  std::pair<CryptoPP::SecByteBlock, CryptoPP::SecByteBlock> _authDigests;

  State _state;
  FrameStripper _frameFilter;

  QTimer _timeout;
};
