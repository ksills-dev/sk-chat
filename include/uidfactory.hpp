#pragma once

#include <functional>
#include <string>
#include <type_traits>
#include <vector>

namespace util {

#define _m_NewUidType(Name)                                                    \
  struct Name##Tag {};                                                         \
  using Name = util::IdBase<Name##Tag>;

// Forward declare
template <typename StrictId> class UidFactory;

/**
 * @brief Base ID Type to be used for simple, strongly typed IDs.
 *
 * To create a new ID type, create an empty Tag struct and alias your
 * specialization.
 *
 * # Example
 * ```
 * struct UidTag;
 * using UidD = IdBase<UIDTag>;
 * ```
 *
 * In this way, we guarantee each ID may only interact those of an equal type,
 * while disallowing overrides that may interfere with the intended design.
 *
 * If you need something else, use something else.
 */
template <typename Tag> class IdBase final {

  friend UidFactory<IdBase<Tag>>; // Allow access to protected functions.

public:
  using TagType = Tag;

  /// Default constructor (value 0)
  IdBase() : _raw(0) {}

  /// ID Constructor from fundamental integer.
  explicit IdBase(std::size_t raw) : _raw(raw) {}

  auto raw() const -> std::size_t { return _raw; }
  auto as_string() const -> std::string { return std::to_string(_raw); }

  /**
   * @brief Basic quality operator. Returns true if the IDs are identical.
   */
  auto operator==(const IdBase<Tag> other) const -> bool {
    return (_raw == other._raw);
  }

  /**
   * @brief Basic inquality operator. Returns false if the IDs are identical.
   */
  auto operator!=(const IdBase<Tag> other) const -> bool {
    return (_raw == other._raw);
  }

  struct hash {
    auto operator()(const IdBase<Tag> &id) const -> std::size_t {
      return std::hash<std::size_t>{}(id._raw);
    }
  };

protected:
  /**
   * @brief
   *
   * This function is protected as it has absolutely no meaning outside of the
   * ID Factory.
   */
  auto operator<(const IdBase<Tag> other) -> bool { return _raw < other._raw; }

private:
  std::size_t _raw; ///< Underlying value of ID.
};

// Compile-time check to ensure a type is specialization of Uid.
template <typename> struct isUidType : std::false_type {};
template <typename Tag> struct isUidType<IdBase<Tag>> : std::true_type {};

/**
 * @brief A generic Factory class to generate specialized ID_Base IDs.
 */
template <typename StrictId> class UidFactory {
public:
  /**
   * @brief Create an empty factory, starting all IDs from 0.
   */
  UidFactory() : _maxId(0) {
    static_assert(
        isUidType<StrictId>::value,
        "Strict_ID is not an alias for the given ID_Base specialization.");
  }

  /**
   * @brief Create a factory from an existing sparsity list and max id.
   *
   * The sparsity_list provided need not be sorted, and may even contain
   * elements above the max_id; it will be optimized internally.
   */
  UidFactory(StrictId max_id, std::vector<StrictId> sparsityList)
      : _maxId(max_id) {
    static_assert(
        isUidType<StrictId>::value,
        "Strict_ID is not an alias for the given ID_Base specialization.");
    // Sort, trim duplicates, trim erraneous.
    sparsityList.sort();
    sparsityList.unique();
    sparsityList.remove_if(
        [max_id](const StrictId &n) { return (n.raw() > max_id); });
  }

  /**
   * @brief Create a factory from a list of used IDs.
   */
  UidFactory(std::vector<StrictId> utilizedList) {
    static_assert(
        isUidType<StrictId>::value,
        "Strict_ID is not an alias for the given ID_Base specialization.");
    // Find our highest used id.
    std::size_t maxId = 0;
    for (const auto &id : utilizedList) {
      if (id.raw() > maxId) {
        maxId = id.raw();
      }
    }

    // For each id up to the max, add a non-existant ID to the sparsity list.
    for (std::size_t id = 0; id < maxId; ++id) {
      bool found = false;
      for (auto it = utilizedList.begin(); it != utilizedList.end(); ++it) {
        if ((*it).raw() == id) {
          found = true;
          utilizedList.erase(it);
          break;
        }
      }

      if (!found) {
        _sparsityList.emplace_back(id);
      }
    }
  }

  /**
   * @brief Generates the next available unique ID.
   */
  auto generateId() -> StrictId {
    if (!_sparsityList.empty()) {
      auto back = *_sparsityList.end();
      _sparsityList.pop_back();
      return StrictId(back);
    } else {
      return StrictId(_maxId++);
    }
  }

  /**
   * @brief Frees the given ID, allowing it to be generated again in the future.
   */
  auto freeId(StrictId id) -> void {
    auto raw = id.raw();
    if (raw > _maxId) {
      return;
    }

    bool foundSpot = false;
    for (auto i = _sparsityList.begin(); i != _sparsityList.end(); ++i) {
      auto free_id = *i;

      if (free_id == raw) {
        return;
      } else if (free_id > raw) {
        _sparsityList.insert(--i, raw);
        foundSpot = true;
      }
    }
    if (!foundSpot) {
      _sparsityList.push_back(raw);
    }
  }

  /**
   * @brief Returns a list of free IDs lower than our max id.
   */
  auto sparsityList() const -> std::vector<StrictId> { return _sparsityList; }

  /**
   * @brief Returns the highest assigned ID so far.
   */
  auto maxId() const -> StrictId { return _maxId; }

private:
  std::vector<std::size_t>
      _sparsityList;  ///< Sorted list of free IDs up to our _max_id.
  std::size_t _maxId; ///< Highest used ID.
};

} // namespace util
