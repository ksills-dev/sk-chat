#pragma once

#include <cstdint>
#include <optional>
#include <string>
#include <utility>
#include <vector>

std::size_t constexpr frameLengthPrefix = 2;

/**
 * @brief Generates a length prefix frame to a given message.
 * @param content The input with an input contract of size < 2^16.
 */
auto generateFrame(std::vector<std::uint8_t> const &content)
    -> std::vector<std::uint8_t>;

class FrameStripper {
public:
  FrameStripper() = default;
  FrameStripper(FrameStripper const &original) = default;
  auto operator=(FrameStripper const &original) -> FrameStripper & = default;
  ~FrameStripper() = default;

  auto bufferRegion() const -> std::pair<std::uint8_t const *, std::size_t>;
  auto bufferRegion() -> std::pair<std::uint8_t *, std::size_t>;
  auto bufferRegion(std::size_t capacity) -> std::uint8_t *;

  auto isReady() const -> bool;

  auto push(std::vector<std::uint8_t> const &input) -> void;
  auto pop() -> std::optional<std::vector<std::uint8_t>>;

private:
  bool _working = false;
  std::uint16_t _messageLength;
  std::vector<std::uint8_t> _buffer;
};
