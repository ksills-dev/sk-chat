#pragma once

#include <forms.hpp>
#include <framestripper.hpp>
#include <uidfactory.hpp>

#include <QHostAddress>
#include <QObject>
#include <QTcpSocket>
#include <QTextCursor>
#include <QTextDocument>
#include <QTimer>

#include <verdigris/wobjectdefs.h>

#include <cryptopp/blake2.h>
#include <cryptopp/secblock.h>

#include <cstdint>

#ifdef NDEBUG
static std::size_t const tumbleInterval = 100;
#else
static std::size_t const tumbleInterval = 3;
#endif

_m_NewUidType(ChatroomId);

struct UserRequestInfo {
  QHostAddress target;
  RequestForm form;
  CryptoPP::SecByteBlock passHash;
};

struct ChannelData {
  CryptoPP::SecByteBlock sKey;
  CryptoPP::SecByteBlock mKey;
  CryptoPP::SecByteBlock aKey;

  QString alias;
  CryptoPP::BLAKE2b runningDigest;
  std::size_t msgCount;
};

enum class ActiveParty { Incoming, Outgoing, None };

W_REGISTER_ARGTYPE(MessageForm)
W_REGISTER_ARGTYPE(ChatroomId)

class Chatroom : public QObject {
  W_OBJECT(Chatroom)

  friend class Handshaker;

public:
  Chatroom();
  Chatroom(Chatroom const &original) = delete;
  auto operator=(Chatroom const &original) = delete;
  ~Chatroom() = default;

  auto alive() const -> bool;
  auto id() const -> ChatroomId;
  auto socket() const -> QTcpSocket const *;
  auto chatLog() -> QTextDocument *;
  auto incomingChannel() const -> ChannelData const *;
  auto outgoingChannel() const -> ChannelData const *;
  auto passHash() const -> CryptoPP::SecByteBlock const *;

  // Slots
  auto start(ChatroomId const id) -> void;
  auto close() -> void;
  auto sendMessage(QString body) -> void;

  // Signals
  auto onClose(ChatroomId port) -> void W_SIGNAL(onClose, port);

  auto onMessageReceived(ChatroomId port, MessageForm message)
      -> void W_SIGNAL(onMessageReceived, port, message);

  auto onTumble(ChatroomId port) -> void W_SIGNAL(onTumble, port);
  auto onPanic(ChatroomId port) -> void W_SIGNAL(onPanic, port);
  auto onDisconnect(ChatroomId port) -> void W_SIGNAL(onDisconnect, port);

private:
  bool _closed = false;

  /**
   * @brief handlePacket
   * @note Currently we silently ignore any problems within incoming forms.
   */
  auto handlePacket() -> void;

  auto handleTimeout() -> void;
  auto handleKeepAlive() -> void;
  auto handlePanic() -> void;
  auto handleDisconnect() -> void;

  auto keyTumble(ChannelData &channel) -> void;
  auto panic() -> void;

  ActiveParty _lastLogged;
  QTextDocument _chatLog;
  QTextCursor _chatCursor;

  QTimer _timeoutClock;
  QTimer _keepAliveClock;

protected:
  ChannelData _inChannel;
  ChannelData _outChannel;

  CryptoPP::SecByteBlock _passHash;
  ChatroomId _id;

  FrameStripper _frameFilter;
  QTcpSocket *_socket = nullptr;
};
