#pragma once

#include <chatroom.hpp>
#include <forms.hpp>

#include <QDialog>
#include <QHostAddress>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

#include <verdigris/wobjectdefs.h>

#include <cryptopp/secblock.h>

#include <optional>

class UserRequestDialog : public QDialog {
  W_OBJECT(UserRequestDialog)

public:
  UserRequestDialog() = delete;
  UserRequestDialog(UserRequestDialog const &) = delete;
  auto operator=(UserRequestDialog const &) -> UserRequestDialog & = delete;
  ~UserRequestDialog() = default;

  static auto ask(std::optional<QHostAddress> target)
      -> std::optional<UserRequestInfo>;

private:
  UserRequestDialog(std::optional<QHostAddress> target);

  auto handleConnectButton() -> void;
  auto handleCancelButton() -> void;

  QLabel *_targetLabel;
  QLineEdit *_targetInput;
  QLabel *_targetErrLabel;

  QLabel *_aliasLabel;
  QLineEdit *_aliasInput;
  QLabel *_aliasErrLabel;

  QLabel *_passLabel;
  QLineEdit *_passInput;
  QLabel *_passErrLabel;

  QPushButton *_cancelButton;
  QPushButton *_connectButton;

  enum class Result { Pending, Cancelled, Submitted };
  Result _state;
};
