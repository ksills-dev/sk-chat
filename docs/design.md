# CIS 6930 - Computer Security

## Overview
Secure messaging app between 2-parties over an arbitrarily secure network.
Key size of only **56-bits** using **DES-CBC** encryption, which necessitates
key tumbling. Due to the weakness of the key, the project becomes an endeavor in
minimizing penetration depth rather than preventing penetration at all.

We authenticate both sides using **Scrypt** password hashes and a
challenger-provided nonce. A secondary hash (using the same nonce) of only
128-bits is used to derive the Master and initial Aux keys. The Master key,
$K_m$, is used for encryption and the Auxiliary key, $K_a$, is used to generate
the integrity digests for messages. After some $n$ messages, the keys are
securely tumbled.

Each side of the communication has it's own **one-way values** such as keys,
running digests, etc and only share the initial password and nonces. This allows
us to circumvent the complexity of synchronizing states for key tumbling
purposes.

Never use this tool in the real world. Nothing short of 256-bit key encryption
should be used, preferably making use of asymmetric keys for key-exchange.

## Dependencies
### Qt::Widgets
**URL:**
**License:** LGPL
**Purpose:** General purpose GUI framework. Networking Sockets.

### Verdigris
**URL:**
**License:**
**Purpose:** Allows us to use Qt slots and signals without the Qt MOC.

### JSON
**URL:**
**License:**
**Purpose:** Simple, modern C++ support for JSON table (de)serialization.

### Crypto++
**URL:**
**License:**
**Purpose:** Provides the cryptographic primitives (DES, CBC, Blake2B, Scrypt).
             Decoding / Encoding for Base64 and Hex.

## The Interface
```plantuml
@startuml
salt
{+
  {* File | Chat | About}
  {/ Dave | Friend2 <&bell> | ...}
  {
    {
      {+
        Connection Established!
        ----
        ----
        Dave
        ----
        What is up my dude?<&tag>
        How are things going?<&tag>
        ----
        Me
        ----
        Not much my dude!<&tag>
        ---
        Dave
        ---
        That's great to hear!<&tag>
        <i><sub>Key Tumble</sub></i>
      }
      ----
      {
        "Type your message here." | [Send]
      }
    } | {+
        <b>Chatroom:</b>
        Password Hash: | ...
        Room ID:       | ...

        <b>User:</b>
        Session Key:    | ...
        Master Key:     | ...
        Current Aux:    | ...
        Running Digest: | ...
        Key TTL:        | ...

        <b>Partner:</b>
        Session Key:    | ...
        Master Key:     | ...
        Current Aux:    | ...
        Running Digest: | ...
        Key TTL:        | ...
    }
  }
}
@enduml
```

### Starting New Chats
We make a request to start a new chat by selecting `Chat > New`. The
dialogue below will be opened.

```plantuml
@startuml
salt
{+
  Target IP: | "127.0.0.1"
  Username:  | "FigNewton"
  Password:  | "*********"
  [Cancel]   | [Connect]
}
@enduml
```

The user should enter an IP address for the second party, a pseudonym that they
would like to use during the chat, and the password they would like to use.
Once established, a new TCP port is reserved and the new chat created in a
handshake state.

The other party receives this request and the following dialogue is presented:

```plantuml
@startuml
salt
{+
  Incoming Chat Request
  IP:       | 192.168.0.1
  Time:     | <TIME>
  Username: | FigNewton
  [Reject]  | [Accept]
}
@enduml
```

They may choose to reject or accept the message. In the case of the former, the
program simply sends the refusal and the dialogue is closed, in the case of the
latter the same "new chat" dialogue is provided (with the IP field grayed out).

After pressing "Connect" the initial party is presented one last incoming
chat request dialog to confirm or deny. If accepted, the handshake is performed
and one of the following prompts displayed.

#### Prompt on Failure
```plantuml
@startuml
salt
{+
  <&warning> Connection Failed
  ----
  <REASON>
  ----
  [Ok]
}
@enduml
```

The failures that bring this prompt are non-security related (could not
establish connection, manual refusal, etc).

#### Prompt on Security Failure
```plantuml
@startuml
salt
{+
  <&shield> Security Guard
  ----
  User and Partner could not agree on Session Key.
  ----
  [Ok]
}
@enduml
```

This prompt is thrown when digests are non-matching. It is *extra* important
that any time this occurs, the nonces used are saved and *never used again*.

#### Prompt on Good Connection
```plantuml
@startuml
salt
{+
  <&check> Connection Secure
  ----
  [Ok]
}
@enduml
```

Yay, you're all set up!

### Closing a Chat
Simply click `Chat > Close`

### Exiting the Program
Simply click `File > Quit`

## Algorithms
### P-SPEKE Generator
$g = Blake2B(P)^2 \% p$

The generator value used for SPEKE handshake is the cryptographic hash of our
password squared and modulo the chosen prime. The squaring is necessary to
prevent small suborder confinement attacks.

### Session Key Generation
$s_a = Blake2B(A | X | T_1)$
$s_b = Blake2B(B | Y | T_2)$
$s_{id} = max(s_a, s_b) | min(s_a, s_b)$
$K_s = Scrypt(g^{xy} \% p, s_{id}, 32, c, b, p)$

If either X or Y are outside of the range $[2 .. p-2]$, the values **must** be
rejected! (Check not implemented).

### Session Key Validation
The first party to validate (who made the request to chat) sends a doubled hash
over the session key. The other party double hashes themselves and ensures the
values match. They then send the single hash of the session key and the original
party validates in the same way. This allows authentication of the final
session key without compromising it's security in any way.

### (Master, Aux) Key Generation
$K_{m0} = ParityCorrect(K_s[0..64))$
$K_{a0} = ParityCorrect(K_s[64..128))$
$K_e = K_s[8i] | K_s[128...256)$

The first and second 64-bits of the session key have their parity bits corrected
(for posterity's sake) and then used for the master and auxiliary keys
respectively.

**Note**: If the resulting keys are "weak" - they need to go through key
tumbling until strong. Currently not implemented.

### Key Tumbling
$K_{s(n)} = Scrypt(P | D_n, K_{s(n-1)})$

When performing a tumble the entire session key is cycled. We throw the password
into scrypt, concatenated with the running hash of all our messages to this
point. We use the prior session key as the salt.

In this way, attackers can't realistically keep up with a conversation they've
already broken into. Even if they're able to break the master and auxiliary
keys, they'll need to know the history of the entire conversation up to this
point. Further, they'll need to know the original password. Using the full
session key as salt also introduces 144-bits (128 from the end, 16 from
overwritten parity bits) of entropy gained in the initial P-SPEKE handshake
that must be bruteforced over this "strong" hashing function.

The master and auxiliary pair is generated identically as before - simply
taking the first and second 64 bits, the remaining 128 bits (and those 16
overwritten correcting their parity) provide entropy for future tumbles.

**Note**: If any of the resulting key are "weak" - they must be rejected and
sent through further tumbling until strong. Currently not implemented.

### Unidirectional Session Keys
In the sake of simplifying client synchronization of key tumbling (preventing
conflicts, relaxing time differential constraints, etc), each direction in the
TCP duplex is assigned it's own session key. The handshake phase therefore
generates a 512-bit session key which is split in two - the first half going
to the initiator's channel and the latter half to the receiver's channel.

### Message Digest
$Digest = Blake2B(M, K_a)$

Keyed Blake2B is secure and extremely fast, so this is perfect for our use case.
Due to the small key size, brute-forcing is a viable attack. However, for one to
be able to spoof messages, they must break both the master encryption key and
the auxiliary validation key separately. This is considered "sufficiently
difficult" for the project at hand - but in a real-world context would require
a significantly larger key size.

We could go further and include the running message digest, adding a constraint
that an attacker would need to break the conversation in-order. However, this
wouldn't add any significant amount of security and it's ability to be easily
brute-forced in this context would remove our attempts at off-line password
protection during key tumbling.

### Connection Handshake
```plantuml
@startuml
skinparam lifelineStrategy solid
skinparam shadowing false
skinparam classFontSize 12
hide footbox

actor Alice
boundary "Alice Client"
entity "Alice Room"
entity "Bob Room"
boundary "Bob Client"
actor Bob

note over "Bob Client" : Listening on Port 1337

== Connection ==
...
Alice -\ "Alice Client" : User Requests Chat
create "Alice Room"
"Alice Client" -> "Alice Room" : Open TCP Socket
"Alice Room" -> "Bob Client" : TCP Connection Request
create "Bob Room"
"Bob Client" --> "Bob Room" : Accept Connection
rnote over "Alice Room", "Bob Room" : TCP Connection Established

== Key Exchange ==

"Alice Room" -[hidden]> "Alice Room"
activate "Alice Room" #Gold
rnote left "Alice Room" : Generate X
"Alice Room" -> "Bob Room" : ""Request Chat { Alias, Time, X }""
deactivate "Alice Room"
"Bob Room" -> Bob : Prompt to Accept
activate Bob #Beige
...
Bob --\ "Bob Room" : Accept
deactivate Bob
"Bob Room" -> Bob : Request Credentials
activate Bob #Beige
...
Bob --\ "Bob Room" : Credentials
deactivate Bob
activate "Bob Room" #Gold
rnote right "Bob Room" : Generate Y
"Bob Room" -\ "Alice Room" : ""Request for Chat { Alias, Time, Y }""
deactivate "Bob Room"
"Bob Room" -[hidden]-> "Bob Room"
activate "Bob Room" #LightSkyBlue
"Alice Room" -> Alice : Prompt to Accept
activate Alice #Beige
...
Alice --\ "Alice Room" : Accept
deactivate Alice
activate "Alice Room" #LightSkyBlue
rnote over "Alice Room", "Bob Room" : Generate Session Key
"Alice Room" -[hidden]-> "Alice Room"
deactivate "Alice Room"
deactivate "Bob Room"

== Key Authentication ==
"Bob Room" -> "Alice Room" : ""Secure_Ack { H(H(K)) }""
activate "Alice Room" #Chartreuse
rnote left "Alice Room" : Validate
"Alice Room" -> "Bob Room" : ""Secure_Ack { H(K) }""
deactivate "Alice Room"

activate "Bob Room" #Chartreuse
rnote right "Bob Room" : Validate
"Bob Room" --> "Alice Room" : Accept
deactivate "Bob Room"

== Chatroom Established ==
@enduml
```

This handshaking protocol takes after the Patched SPEKE (P-SPEKE) protocol to
allow for opaque key exchange without compromising offline password security.
We include the timestamp for each request as further protection against
the impersonation attack vulnerabilities outlined in our P-SPEKE reference.
Not shown here, the alias provided by each user is appended with an 8-character
base64 encoded nonce. This is not displayed to the user (except in the
detailed stats view), but adds an additional layer of automated protection on
the P-SPEKE protocol.

### Message Exchange
The following diagram outlines the simple means of a successful message
exchange.

```plantuml
@startuml
"Alice Room" -> "Bob Room" : ""Message { Body, Time, AuthCode }""
hnote over "Bob Room" : Validate
@enduml
```

If the message fails to validate, we panic abort (message tampering detected,
only a matter of time until the keys have been broken).

Occasionally, if a party has not recently sent a message their client will send
a keep-alive signal. If several scheduled keep-alive signals are missed, the
partner client will panic abort (unreliable connection detected, may be
attempted man in the middle).

## Forms
Forms are sent as JSON table strings. Each of these tables is
prepended by a 16-bit message length designator (limiting the size of our
messages to 2^16 each, though we limit the contents to 2^15) for message
framing.

### Unencrypted
The following forms are sent completely raw and unencrypted, they contain no
sensitive information.

#### Request for Chat
```json
{
  "form"      : "chat-request",
  "alias"     : "<NAME>",
  "timestamp" : "<TIME>",
  "public"    : "<PUBLIC-KEY>"
}
```

#### Reject Chat Request
```json
{
  "form": "reject",
  "why" : "<CAUSE>"
}
```

#### Secure Chat Acknowledge
```json
{
  "form"  : "sec-ack",
  "digest": "<PK-DIGEST>"
}
```

### Encrypted
The following messages are encrypted in single-DES CBC mode using the master
key.

#### Message
```json
{
  "form"     : "message",
  "message"  : "<MSG>",
  "timestamp": "<UTC>",
  "auth"     : "<MSG-DIGEST>"
}
```

#### Panic
```json
{
  "form": "panic",
}
```

#### Keep Alive
```json
{
  "form": "keep-alive"
}
```

## Architecture
